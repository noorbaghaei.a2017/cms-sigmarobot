<?php
return [
    "text-create"=>"you can create your education",
    "text-edit"=>"you can edit your education",
    "store"=>"Store Success",
    "delete"=>"Delete Success",
    "update"=>"Update Success",
    "index"=>"educations list",
    "error"=>"error",
    "singular"=>"education",
    "collect"=>"educations",
    "permission"=>[
        "education-full-access"=>"education full access",
        "education-list"=>"education list",
        "education-delete"=>"education delete",
        "education-create"=>"education create",
        "education-edit"=>"edit education",
    ]


];
