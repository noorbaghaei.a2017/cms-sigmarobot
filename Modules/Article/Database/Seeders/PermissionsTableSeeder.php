<?php

namespace Modules\Article\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Modules\Article\Entities\Article;
use Spatie\Permission\Models\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::table('permissions')->whereModel(Article::class)->delete();

        Permission::create(['name'=>'article-list','model'=>Article::class,'created_at'=>now()]);
        Permission::create(['name'=>'article-create','model'=>Article::class,'created_at'=>now()]);
        Permission::create(['name'=>'article-edit','model'=>Article::class,'created_at'=>now()]);
        Permission::create(['name'=>'article-delete','model'=>Article::class,'created_at'=>now()]);

    }
}
