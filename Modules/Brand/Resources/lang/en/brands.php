<?php
return [
    "text-create"=>"you can create your brand",
    "text-edit"=>"you can edit your brand",
    "store"=>"Store Success",
    "delete"=>"Delete Success",
    "update"=>"Update Success",
    "index"=>"brands list",
    "singular"=>"brand",
    "collect"=>"brands",
    "permission"=>[
        "brand-full-access"=>"brand full access",
        "brand-list"=>"brands list",
        "brand-delete"=>"brand delete",
        "brand-create"=>"brand create",
        "brand-edit"=>"edit customer",
    ]
];
