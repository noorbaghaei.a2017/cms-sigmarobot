<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientChatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_chats', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('client')->unsigned();
            $table->foreign('client')->references('id')->on('clients')->onDelete('cascade');
            $table->bigInteger('user')->nullable()->unsigned();
            $table->foreign('user')->references('id')->on('users');
            $table->text('text')->nullable();
            $table->tinyInteger('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_chats');
    }
}
