<?php
return [
    "text-create"=>"you can create your comment",
    "text-edit"=>"you can edit your comment",
    "store"=>"Store Success",
    "delete"=>"Delete Success",
    "update"=>"Update Success",
    "index"=>"comments list",
    "singular"=>"comment",
    "collect"=>"comments",
    "permission"=>[
        "questions-full-access"=>"comments full access",
        "questions-list"=>"comments list",
        "questions-delete"=>"comment delete",
        "questions-create"=>"comment create",
        "questions-edit"=>"edit comment",
    ]
];
