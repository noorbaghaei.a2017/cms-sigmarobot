<?php

namespace Modules\Core\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Modules\Core\Entities\Setting;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('settings')->delete();

        DB::table('templates')->delete();

        $settings=[

            [
                'name' => 'sigmarobot',
                'address' => 'Iran',
                'domain' => 'http://sigmarobot.ir',
                'country' => 'Iran',
                'email' => 'support@sigmarobot.ir',
                'phone' => json_encode(['02134567','0214568329']),
                'pre_phone' => '+98',
                'mobile' => json_encode(['09195995044','09123456751','09195432910'])
            ]
        ];

        $templates=[

            [
                'title' => 'sigmarobot',
                'description' => 'sigmarobot template',
                'status' => 1
            ]
        ];

        DB::table('settings')->insert($settings);

        DB::table('templates')->insert($templates);

        $setting=Setting::with('info')->firstOrFail();

        $setting->seo()->create([
            'title'=>'',
            'description'=>'',
            'keyword'=>'',
            'robots'=>json_encode(['index','follow']),
            'canonical'=>env('APP_URL'),
        ]);
        $setting->info()->create([
            'telegram'=>'',
            'instagram'=>'',
            'whatsapp'=>''
        ]);
    }
}
