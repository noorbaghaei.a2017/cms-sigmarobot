<?php

namespace Modules\Core\Entities;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Modules\Advertising\Entities\Advertising;
use Modules\Core\Helper\CategoryHelper;
use Modules\Core\Helper\Trades\CommonAttribute;
use Modules\Core\Helper\Trades\TimeAttribute;
use Modules\Product\Entities\Product;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class Category extends Model implements HasMedia
{
    use TimeAttribute,CommonAttribute,Sluggable,HasMediaTrait;

    protected $fillable = ['parent','status','level','icon','model','slug','excerpt','token','order','user','title','symbol'];

    protected $table='categories';

    public function getRouteKeyName()
    {
        return multiRouteKey();
    }
    public function advertisings(){

        return $this->hasMany(Advertising::class,'category');
    }
    public function analyzer()
    {
        return $this->morphOne(Analyzer::class, 'analyzerable');
    }
    public function products(){

        return $this->hasMany(Product::class,'category','id');
    }

    public  function getShowStatusAttribute(){
        return CategoryHelper::status($this->status);
    }

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('medium')
            ->width(300)
            ->height(300)
            ->performOnCollections(config('cms.collection-image'));

        $this->addMediaConversion('thumb')
            ->width(50)
            ->height(50)
            ->performOnCollections(config('cms.collection-image'));
    }

    public  function getViewAttribute(){

        return $this->analyzer->view;
    }

    public  function getLikeAttribute(){

        return $this->analyzer->like;
    }

    /**
     * @inheritDoc
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }



}
