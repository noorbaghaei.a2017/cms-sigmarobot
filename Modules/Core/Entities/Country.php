<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $fillable = [];

    protected $table ="countries";
}
