<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;

class Discount extends Model
{
    protected $fillable = ['title','amount','percentage','start_at','end_at'];

}
