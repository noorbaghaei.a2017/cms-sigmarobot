<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    protected $fillable = ['country_id'];

    protected $table ="states";
}
