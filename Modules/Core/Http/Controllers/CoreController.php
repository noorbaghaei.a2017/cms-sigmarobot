<?php

namespace Modules\Core\Http\Controllers;

use Illuminate\Http\Response;
use Illuminate\Routing\Controller;


class CoreController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        try {
            return view('core::dashboard');
        }catch (\Exception $exception){

        }
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function meeting()
    {
        try {
            return view('core::layout.meeting');
        }catch (\Exception $exception){

        }
    }
    public function myDesk()
    {
        try {
            return view('core::member.dashboard');
        }catch (\Exception $exception){

        }
    }
    public function login()
    {
        try {
            return view('core::member.auth.login');
        }catch (\Exception $exception){

        }
    }



}
