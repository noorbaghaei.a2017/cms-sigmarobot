@include('core::layout.modules.index',[

    'title'=>__('core::contacts.index'),
    'items'=>$items,
    'parent'=>'core',
    'model'=>'contact',
    'directory'=>'contact',
    'collect'=>__('core::contacts.collect'),
    'singular'=>__('core::contacts.singular'),
    'edit_route'=>['name'=>'contacts.edit','name_param'=>'contact'],
    'destroy_route'=>['name'=>'contacts.destroy','name_param'=>'contact'],
    'search_route'=>true,
    'datatable'=>[
    __('cms.name')=>'name',
    __('cms.last_name')=>'last_name',
    __('cms.email')=>'email',
    __('cms.create_date')=>'AgoTime'
    ],
        'detail_data'=>[
    __('cms.name')=>'name',
    __('cms.last_name')=>'last_name',
    __('cms.email')=>'email',
    __('cms.text')=>'message',
    __('cms.create_date')=>'created_at'
    ],


])
