@include('core::layout.header')
<body>
<div class="app" id="app">
    <div id="aside" class="app-aside fade nav-dropdown black">
        <div class="navside dk" data-layout="column">

            <div class="navbar no-radius">
                <div>
                    @if(env('SET_CUSTOMER'))
                        <img src="{{asset('assets/images/ads/roshan-logo.jpeg')}}" width="200"  style="margin: 0 auto">
                    @else
                        <video autoplay id="video" style="width:200px"></video>

                    @endif
                    <br>
                </div>
                <a href="#" class="navbar-brand">


                    @if(!$setting->Hasmedia('logo'))
                        <img src="{{asset('assets/images/logo.png')}}" alt="{{config('app.name')}}" >
                    @else
                        <img src="{{$setting->getFirstMediaUrl('logo')}}" alt="{{config('app.name')}}" >
                    @endif

                        <span class="hidden-folded inline">{{$setting->name}}</span>
                    <br>
                    <span class="hidden-folded inline">{{number_format($total_orders)}}   تومان </span>
                    <br>
                    <span class="hidden-folded inline">{{showIp()}}</span>
                </a>
            </div>

@include('core::layout.list-module')
        </div>
    </div>
    <div id="content" class="app-content box-shadow-z2 bg pjax-container" role="main">
        <div class="app-header white bg b-b">
            <div class="navbar" data-pjax>
                <a data-toggle="modal" data-target="#aside" class="navbar-item pull-left hidden-lg-up p-r m-a-0">
                    <i class="ion-navicon"></i>
                </a>
                <div class="navbar-item pull-left h5" id="pageTitle">@yield('pageTitle')</div>
                <ul class="nav navbar-nav pull-right">
                    <li class="nav-item"><a href="{{route('meeting.website')}}" target="_blank" class="btn btn-sm text-sm btn-success text-white m-t-1">{{__('cms.chat-room')}}</a></li>
                    <li class="nav-item"><a href="{{route('front.website')}}" target="_blank" class="btn btn-sm text-sm btn-primary text-white m-t-1">{{__('cms.website')}}</a></li>
                    <li class="nav-item dropdown">
                        <a class="nav-link clear" data-toggle="dropdown">
                            <span class="avatar w-32">
                                <img src="{{asset('assets/images/a3.jpg')}}" class="w-full rounded" alt="#">
                            </span>
                        </a>
                        <div class="dropdown-menu w dropdown-menu-scale pull-right">
                            <a class="dropdown-item" href="{{route('user.profile',['user'=>auth('web')->user()->token])}}">
                                <span>{{__('cms.profile')}}</span>
                            </a>
                            <a class="dropdown-item"
                               onclick="event.preventDefault(); document.getElementById('frm-logout').submit();">{{__('cms.logout')}}</a>
                            <form id="frm-logout" action="{{route('logout')}}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </div>
                    </li>
                </ul>
            </div>
            <div>
                <h6 class="text-center" style="padding: 5px">
                    {{__('cms.text-top-dashboard')}}
                </h6>

            </div>
        </div>
@include('core::layout.copy')
        <div class="app-body">
        <div class="padding">
            <div class="row">
{{--                @include('core::layout.modules.holiday-today')--}}
            </div>
            <div class="row">
                @yield('content')
            </div>
        </div>
        </div>
    </div>
</div>
@include('core::layout.footer')

