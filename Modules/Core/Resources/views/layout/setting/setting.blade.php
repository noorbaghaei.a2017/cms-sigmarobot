@extends('core::layout.panel')
@section('pageTitle', __('cms.setting'))
@section('content')
    <div class="padding">
        <div class="row">
            <div class="col-md-12">
                <div class="box p-a-xs">
                    <div class="row">
                        <div class="col-md-5">
                            <a href="#">
                                @if(!$item->Hasmedia('logo'))
                                    <img style="width: 300px;height: auto" src="{{asset('img/no-img.gif')}}">

                                @else

                                    <img style="width: 400px;height: auto" src="{{$item->getFirstMediaUrl('logo')}}" alt="" class="img-responsive">


                                @endif


                            </a>
                        </div>
                        <div class="col-md-7">
                            <div style="padding-top: 35px">
                                <h6 style="padding-top: 35px"> {{__('cms.title-website')}}  : </h6>
                                <h4 style="padding-top: 35px"> {{$item->name}}</h4>
                            </div>


                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header">
                        <small>
                            با استفاده از فرم زیر میتوانید تنظیمات خود را مشاهده کنید.
                        </small>
                        </div>
                        <div class="box-divider m-a-0"></div>
                        <div class="box-body">
                            @if(session()->has('message'))
                                <div class="alert alert-success">
                                    {{ session()->get('message') }}
                                </div>
                            @endif
                                @include('core::layout.alert-danger')
                            @if(\Illuminate\Support\Facades\Session::has('error'))
                                <div class="alert alert-danger">
                                    <ul>
                                        <li>{{\Illuminate\Support\Facades\Session::get('error')}}</li>
                                    </ul>
                                </div>
                            @endif

                                <form id="signupForm" action="{{route('dashboard.setting.update')}}" method="POST" enctype="multipart/form-data">
                                    {{csrf_field()}}
                                    {{method_field('PATCH')}}
                                <div class="form-group row">
                                    <div class="col-sm-3">
                                        <label for="name" class="form-control-label">{{__('cms.name')}} </label>
                                        <input type="text" name="name" class="form-control" id="name" value="{{$item->name}} " required>
                                    </div>
                                    <div class="col-sm-3">
                                        <label for="domain" class="form-control-label">{{__('cms.domain')}} </label>
                                        <input type="text" name="domain" class="form-control" id="domain" value="{{$item->domain}} " required>
                                    </div>
                                    <div class="col-sm-3">
                                        <label for="email" class="form-control-label"> {{__('cms.email')}}</label>
                                        <input type="email" name="email" class="form-control" id="email" value="{{$item->email}} " required>
                                    </div>

                                    <div class="col-sm-3">
                                        <label for="title" class="form-control-label">{{__('cms.thumbnail')}} </label>
                                        @include('core::layout.load-single-image')
                                    </div>

                                </div>

                                    <div class="form-group row">
                                        <div class="col-sm-12">
                                            <span class="text-danger">*</span>
                                            <label for="excerpt" class="form-control-label">{{__('cms.excerpt')}} </label>
                                            <input type="text" name="excerpt" value="{{$item->excerpt}}" class="form-control" id="excerpt" autocomplete="off" required>
                                        </div>


                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-12">
                                            <label for="phones"  class="form-control-label">  {{__('cms.phone')}}  </label>
                                            <select dir="rtl" class="form-control" multiple="multiple" id="phones" name="phones[]" required>
                                    @if(!is_null($item->phone))

                                                    @foreach(json_decode($item->phone,true) as $value)
                                                        <option selected>{{$value}}</option>
                                                    @endforeach
                                    @endif
                                            </select>

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-12">
                                            <label for="faxes"  class="form-control-label">  {{__('cms.fax')}}  </label>
                                            <select dir="rtl" class="form-control" multiple="multiple" id="faxes" name="faxes[]" >

                                                    @if(!is_null(json_decode($item->fax)))
                                                    @foreach(json_decode($item->fax,true) as $value)
                                                        <option selected>{{$value}}</option>
                                                    @endforeach
                                                @endif
                                            </select>

                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-sm-12">
                                            <label for="address" class="form-control-label">{{__('cms.address')}} </label>
                                            <input type="text" value="{{$item->address}}" name="address" class="form-control" id="address" autocomplete="off">
                                        </div>


                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-12">
                                            <label for="mobiles"  class="form-control-label">  {{__('cms.mobile')}}  </label>
                                            <select dir="rtl" class="form-control" multiple="multiple" id="mobiles" name="mobiles[]" required>

                                                @if(!is_null(json_decode($item->mobile)))
                                                @foreach(json_decode($item->mobile,true) as $value)
                                                    <option selected>{{$value}}</option>
                                                @endforeach
                                                    @endif
                                            </select>

                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-sm-12">
                                            <label for="copy" class="form-control-label">{{__('cms.copy-right')}} </label>
                                            <input type="text" value="{{$item->copy_right}}" name="copy" class="form-control" id="copy" autocomplete="off">
                                        </div>


                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-6">
                                            <label for="ios_app" class="form-control-label">{{__('cms.ios_app')}} </label>
                                            <input type="text" value="{{$item->ios_app}}" name="ios_app" class="form-control" id="ios_app" autocomplete="off">
                                        </div>
                                        <div class="col-sm-6">
                                            <label for="android_app" class="form-control-label">{{__('cms.android_app')}} </label>
                                            <input type="text" value="{{$item->android_app}}" name="android_app" class="form-control" id="android_app" autocomplete="off">
                                        </div>


                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-6">
                                            <label for="longitude" class="form-control-label">{{__('cms.google-map-longitude')}} </label>
                                            <input type="text" value="{{$item->longitude}}" name="longitude" class="form-control" id="longitude" autocomplete="off">
                                        </div>
                                        <div class="col-sm-6">
                                            <label for="latitude" class="form-control-label">{{__('cms.google-map-latitude')}} </label>
                                            <input type="text" value="{{$item->latitude}}" name="latitude" class="form-control" id="latitude" autocomplete="off">
                                        </div>


                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-12">
                                            <label for="map" class="form-control-label">{{__('cms.google-map-link')}} </label>
                                            <input type="text" value="{{$item->google_map}}" name="map" class="form-control" id="map" autocomplete="off">
                                        </div>


                                    </div>

                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                            <label for="color_theme" class="form-control-label">{{__('cms.color')}} </label>
                                            <input type="text" name="color_theme" class="form-control" id="color_theme" value="{{$item->color_theme}} " required>
                                        </div>


                                    </div>


                                    @include('core::layout.modules.info-box',['info'=>$item->info])

                                    @include('core::layout.modules.seo-box',['seo'=>$item->seo])



                                <div class="form-group row m-t-md">
                                    <div class="col-sm-12">
                                        <button type="submit" class="btn btn-primary btn-sm text-sm">{{__('cms.update')}} </button>
                                    </div>
                                </div>
                            </form>
                            <hr/>
                            <br/>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endsection





@section('heads')

    <link href="{{asset('assets/css/validate/screen.css')}}" rel="stylesheet"/>

@endsection()

@section('scripts')


    <script src="{{asset('assets/scripts/validate/jquery.validate.js')}}"></script>


    <script>


        $().ready(function() {
            // validate the comment form when it is submitted
            $("#commentForm").validate();

            // validate signup form on keyup and submit
            $("#signupForm").validate({
                rules: {
                    name: {
                        required: true
                    },
                    domain: {
                        required: true
                    },
                    country: {
                        required: true
                    },
                    email: {
                        required: true
                    },
                    mobile: {
                        required: true
                    },
                    phone: {
                        required: true
                    },


                },
                messages: {
                    name:"عنوان الزامی است",
                    domain: "دامنه  لزامی است",
                    email: "ایمیل  الزامی است",
                    mobile: "موبایل  الزامی است",
                    phone: "تلفن  الزامی است",
                    country: "کشور الزامی است",
                }
            });


            //code to hide topic selection, disable for demo
            var newsletter = $("#newsletter");
            // newsletter topics are optional, hide at first
            var inital = newsletter.is(":checked");
            var topics = $("#newsletter_topics")[inital ? "removeClass" : "addClass"]("gray");
            var topicInputs = topics.find("input").attr("disabled", !inital);
            // show when newsletter is checked
            newsletter.click(function() {
                topics[this.checked ? "removeClass" : "addClass"]("gray");
                topicInputs.attr("disabled", !this.checked);
            });
        });
    </script>

@endsection
