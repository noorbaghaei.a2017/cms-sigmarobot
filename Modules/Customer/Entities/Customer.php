<?php

namespace Modules\Customer\Entities;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Modules\Core\Entities\Info;
use Modules\Core\Helper\Trades\TimeAttribute;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\Permission\Models\Role;

class Customer extends Model implements HasMedia
{
    use Sluggable,HasMediaTrait,TimeAttribute;

    protected $fillable = ['title','text','excerpt','token','slug','order','user'];

    public function getRouteKeyName()
    {
        return multiRouteKey();
    }

    /**
     * @inheritDoc
     */
    public function sluggable(): array
    {
        return [
            'slug'=>[
               'source'=>'title'
            ]
        ];
    }
    public  function info(){
       return $this->morphOne(Info::class,'infoable');
    }

}
