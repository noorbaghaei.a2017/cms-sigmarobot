

    <li>
        <a href="{{route('customers.index')}}" class="b-success">
                              <span class="nav-icon text-white no-fade">
                                <i class="{{config('customer.icons.customer')}}"></i>
                              </span>
            <span class="nav-text"> {{__('customer::customers.collect')}}</span>
        </a>
    </li>

