@include('core::layout.modules.category.create',[

    'title'=>__('core::categories.create'),
    'parent'=>'educational',
    'model'=>'classroom',
    'directory'=>'classrooms',
    'collect'=>__('core::categories.collect'),
    'singular'=>__('core::categories.singular'),
   'store_route'=>['name'=>'classroom.category.store'],

])








