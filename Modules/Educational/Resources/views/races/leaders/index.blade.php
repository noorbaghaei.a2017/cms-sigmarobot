@include('core::layout.modules.index',[

    'title'=>__('core::leaders.index'),
    'items'=>$items,
    'parent'=>'educational',
    'model'=>'race',
    'directory'=>'races',
    'collect'=>__('core::leaders.collect'),
    'singular'=>__('core::leaders.singular'),
   'create_route'=>['name'=>'race.leader.create'],
    'edit_route'=>['name'=>'race.leader.edit','name_param'=>'leader'],
    'pagination'=>false,
    'parent_route'=>true,
    'datatable'=>[
    __('cms.title')=>'title',
   __('cms.update_date')=>'AgoTimeUpdate',
    __('cms.create_date')=>'TimeCreate',
    ],
        'detail_data'=>[
    __('cms.title')=>'title',
   __('cms.create_date')=>'created_at',
    __('cms.update_date')=>'updated_at',
    ],


])



