<?php


namespace Modules\Member\Helper;


use Modules\Member\Entities\MemberRole;

class MemberHelper
{
    public static function role($role){

                $label=MemberRole::whereId($role)->first()->label;
                return '<span class="status-success">'.$label.'</span>';


    }
    public static function roleName($role){

        return $label=MemberRole::whereId($role)->first()->label;


    }
    public static function status($status){
        switch ($status){
            case 1 :
                return '<span class="alert-success">فعال</span>';
                break;
            case 0 :
                return '<span class="alert-danger">غیر فعال</span>';
                break;
            default:
                return '<span class="alert-danger">خطای سیستمی</span>';
                break;

        }
    }
    public static function statusValue($status)
    {
        switch ($status) {
            case 1 :
                return 'فعال';
                break;
            case 0 :
                return ' غیر فعال';
                break;

            default:
                return 'خطای سیستمی';
                break;

        }
    }


}
