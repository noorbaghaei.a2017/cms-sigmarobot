@extends('core::layout.panel')
@section('pageTitle', __('cms.edit'))
@section('content')
    <div class="padding">
        <div class="row">
            <div class="col-md-12">
                <div class="box p-a-xs">
                    <div class="row">
                        <div class="col-md-5">
                            <a href="#">
                                @if(!$item->Hasmedia('images'))

                                    <img style="width: 400px;height: auto" src="{{asset('img/no-img.gif')}}" alt="" class="img-responsive">


                                @else
                                    <img style="width: 400px;height: auto" src="{{$item->getFirstMediaUrl(config('cms.collection-image'))}}" alt="" class="img-responsive">

                                @endif


                            </a>
                        </div>
                        <div class="col-md-7">
                            <div style="padding-top: 35px">
                                <h6 style="padding-top: 35px"> {{__('cms.full_name')}} : </h6>
                                <h4 style="padding-top: 35px">    {{fullName($item->first_name,$item->last_name)}}</h4>
                            </div>
                            <div>
                                <h6 style="padding-top: 35px"> {{__('cms.role')}} : </h6>
                                <p> {{$item->rolename}}</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    @include('core::layout.alert-danger')
                    <div class="box-header">
                        <div class="pull-left">

                            <small>
                                {{__('member::members.text-edit')}}
                            </small>
                        </div>
                        <a onclick="window.print()" class="btn btn-primary btn-sm text-sm text-white pull-right">{{__('cms.print')}} </a>
                    </div>
                    <br>
                    <br>
                    <div class="box-divider m-a-0"></div>
                    <div class="box-body">
                        <form id="signupForm" role="form" method="POST" action="{{route('members.update',['member'=>$item->token])}}" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" value="{{$item->token}}" name="token">
                            @method('PATCH')
                            <div class="form-group row">
                                <div class="col-sm-12">
                                    <label for="title" class="form-control-label">{{__('cms.thumbnail')}}</label>
                                    @include('core::layout.load-single-image')
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <span class="text-danger">*</span>
                                    <label for="firstname" class="form-control-label">{{__('cms.first_name')}}  </label>
                                    <input type="text" value="{{$item->first_name}}" name="firstname" class="form-control" id="firstname">
                                </div>
                                <div class="col-sm-3">
                                    <span class="text-danger">*</span>
                                    <label for="lastname" class="form-control-label">{{__('cms.last_name')}} </label>
                                    <input type="text" value="{{$item->last_name}}" name="lastname" class="form-control" id="lastname">
                                </div>
                                <div class="col-sm-3">
                                    <span class="text-danger">*</span>
                                    <label for="role"  class="form-control-label">{{__('cms.role')}}  </label>
                                    <select dir="rtl" class="form-control" name="role" >

                                        @foreach($roles as $role)
                                            <option value="{{$role->token}}" {{$item->role==$role->id ?  'selected':''}}>{{__('cms.'.$role->title)}}</option>
                                        @endforeach



                                    </select>

                                </div>


                                <div class="col-sm-3">
                                    <span class="text-danger">*</span>
                                    <label for="order" class="form-control-label">{{__('cms.order')}} </label>
                                    <input type="number" value="{{$item->order}}" name="order" class="form-control" id="order">
                                </div>


                            </div>
                            <div class="form-group row">

                                <div class="col-sm-3">
                                    <span class="text-danger">*</span>
                                    <label for="mobile" class="form-control-label">{{__('cms.mobile')}} </label>
                                    <input type="text" value="{{$item->mobile}}" name="mobile" class="form-control" id="mobile">
                                    <small class="help-block text-warning">09195995044</small>
                                </div>
                                <div class="col-sm-3">
                                    <span class="text-danger">*</span>
                                    <label for="email" class="form-control-label">{{__('cms.email')}} </label>
                                    <input type="email" value="{{$item->email}}" name="email" class="form-control" id="email">
                                </div>
                                <div class="col-sm-3">
                                    <span class="text-danger">*</span>
                                    <label for="password" class="form-control-label">{{__('cms.password')}} </label>
                                    <input type="text" name="password"  class="form-control" id="password" >
                                </div>
                                <div class="col-sm-3">
                                    <span class="text-danger">*</span>
                                    <label for="status" class="form-control-label">{{__('cms.status')}}  </label>
                                    <select dir="rtl" class="form-control" id="status" name="status" required>

                                        <option  value="1" {{$item->status==1 ? "selected" : ""}}>{{__('cms.active')}}</option>
                                        <option  value="0" {{$item->status==0 ? "selected" : ""}}>{{__('cms.inactive')}}</option>


                                    </select>
                                </div>

                            </div>


                            @include('core::layout.modules.info-box',['info'=>$item->info])

                            @include('core::layout.modules.seo-box',['seo'=>$item->seo])

                            <div class="form-group row m-t-md">
                                <div class="col-sm-12">
                                    <input type="submit"  class="btn btn-success btn-sm text-sm" value="{{__('cms.update')}}">
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection




@section('heads')

    <link href="{{asset('assets/css/validate/screen.css')}}" rel="stylesheet"/>

@endsection()

@section('scripts')


    <script src="{{asset('assets/scripts/validate/jquery.validate.js')}}"></script>


    <script>

        $().ready(function() {
            // validate the comment form when it is submitted
            $("#commentForm").validate();

            // validate signup form on keyup and submit
            $("#signupForm").validate({
                rules: {
                    firstname: {
                        required: true
                    },
                    lastname: {
                        required: true
                    },
                    mobile: {
                        required: true,
                        number: true
                    },
                    email: {
                        required: true,
                        email:true
                    },
                    role: {
                        required: true
                    },
                    order: {
                        number: true,
                        required:true
                    },


                },
                messages: {
                    firstname:"نام الزامی است",
                    lastname: "نام خانوادگی  لزامی است",
                    mobile: "موبایل  الزامی است",
                    email: "ایمیل  الزامی است",
                    role: "نقش  الزامی است",
                    order: "فرمت نادرست",
                }
            });


            //code to hide topic selection, disable for demo
            var newsletter = $("#newsletter");
            // newsletter topics are optional, hide at first
            var inital = newsletter.is(":checked");
            var topics = $("#newsletter_topics")[inital ? "removeClass" : "addClass"]("gray");
            var topicInputs = topics.find("input").attr("disabled", !inital);
            // show when newsletter is checked
            newsletter.click(function() {
                topics[this.checked ? "removeClass" : "addClass"]("gray");
                topicInputs.attr("disabled", !this.checked);
            });
        });
    </script>

@endsection
