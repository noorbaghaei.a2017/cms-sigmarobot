<?php


namespace Modules\Payment\Http\Controllers\Facade;


use Illuminate\Support\Facades\Facade;

class ZarinPalFacade extends Facade
{

    protected static function getFacadeAccessor()
    {
        return 'zarinpal';
    }
}
