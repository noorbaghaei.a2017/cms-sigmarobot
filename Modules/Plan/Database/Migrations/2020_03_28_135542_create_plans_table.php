<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user');
            $table->foreign('user')->references('id')->on('users')->onDelete('cascade');
            $table->unsignedBigInteger('period');
            $table->foreign('period')->references('id')->on('periods')->onDelete('cascade');
            $table->string('title')->unique();
            $table->text('text');
            $table->string('slug');
            $table->string('icon')->nullable();
            $table->integer('order')->nullable()->default(1);
            $table->tinyInteger('time_limit')->default(30);
            $table->tinyInteger('number_limit')->default(1);
            $table->tinyInteger('number_limit_special')->default(1);
            $table->tinyInteger('special')->default(1)->nullable();
            $table->string('excerpt');
            $table->tinyInteger('status')->default(1);
            $table->json('attributes')->nullable();
            $table->string('token')->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plans');
    }
}
