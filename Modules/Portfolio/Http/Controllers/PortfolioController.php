<?php

namespace Modules\Portfolio\Http\Controllers;



use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Mockery\Exception;
use Modules\Core\Entities\Category;
use Modules\Core\Http\Controllers\HasCategory;
use Modules\Core\Http\Controllers\HasGallery;
use Modules\Core\Http\Controllers\HasQuestion;
use Modules\Core\Http\Requests\CategoryRequest;
use Modules\Customer\Entities\Customer;
use Modules\Portfolio\Entities\Portfolio;
use Modules\Portfolio\Entities\Repository\PortfolioRepositoryInterface;
use Modules\Portfolio\Http\Requests\PortfolioRequest;
use Modules\Portfolio\Transformers\PortfolioCollection;
use Modules\Question\Entities\Question;
use Modules\Question\Http\Requests\QuestionRequest;
use Spatie\MediaLibrary\Models\Media;

class PortfolioController extends Controller
{

    use HasQuestion,HasCategory,HasGallery;

    protected $entity;

    protected $class;

    private $repository;

//category

    protected $route_categories_index='portfolio::categories.index';
    protected $route_categories_create='portfolio::categories.create';
    protected $route_categories_edit='portfolio::categories.edit';
    protected $route_categories='portfolio.categories';

//question

    protected $route_questions_index='portfolio::questions.index';
    protected $route_questions_create='portfolio::questions.create';
    protected $route_questions_edit='portfolio::questions.edit';
    protected $route_questions='portfolios.index';


//gallery

    protected $route_gallery_index='portfolio::portfolios.gallery';
    protected $route_gallery='portfolios.index';



//notification

    protected $notification_store='portfolio::portfolios.store';
    protected $notification_update='portfolio::portfolios.update';
    protected $notification_delete='portfolio::portfolios.delete';
    protected $notification_error='portfolio::portfolios.error';




    public function __construct(PortfolioRepositoryInterface $repository)
    {
        $this->entity=new Portfolio();

        $this->class=Portfolio::class;

        $this->repository=$repository;

        $this->middleware('permission:portfolio-list')->only('index');
        $this->middleware('permission:portfolio-create')->only(['create','store']);
        $this->middleware('permission:portfolio-edit' )->only(['edit','update']);
        $this->middleware('permission:portfolio-delete')->only(['destroy']);
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        try {

            $items=$this->repository->getAll();

            $portfolios = new PortfolioCollection($items);

            $data= collect($portfolios->response()->getData())->toArray();

            return view('core::response.index',compact('data'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        try {
            $categories=Category::latest()->where('model',Portfolio::class)->get();
            $customers=Customer::latest()->get();
            return view('portfolio::portfolios.create',compact('customers','categories'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }
    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return Response
     */
    public function search(Request $request)
    {
        try {
            if(
                !isset($request->title) &&
                !isset($request->slug)
            ){
                $items=$this->repository->getAll();

                $result = new PortfolioCollection($items);

                $data= collect($result->response()->getData())->toArray();

                return view('core::response.index',compact('data'));
            }
            $items=$this->entity
                ->where("title",trim($request->title))
                ->where("slug",trim($request->slug))
                ->paginate(config('cms.paginate'));
            $result = new PortfolioCollection($items);

            $data= collect($result->response()->getData())->toArray();

            return view('core::response.index',compact('data'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param PortfolioRequest $request
     * @return Response
     */
    public function store(PortfolioRequest $request)
    {
        try {
            $customer=Customer::whereToken($request->input('customer'))->firstOrFail();
            $this->entity->user=auth('web')->user()->id;
            $this->entity->title=$request->input('title');
            $this->entity->category=Category::whereToken($request->input('category'))->first()->id;
            $this->entity->excerpt=$request->input('excerpt');
            $this->entity->text=$request->input('text');
            $this->entity->customer=$customer->id;
            $this->entity->order=orderInfo($request->input('order'));;
            $this->entity->token=tokenGenerate();

            $saved=$this->entity->save();

            $this->entity->seo()->create([
                'title'=>$request->input('title-seo'),
                'description'=>$request->input('description-seo'),
                'keyword'=>$request->input('keyword-seo'),
                'canonical'=>$request->input('canonical-seo'),
                'robots'=>(is_null(json_encode($request->input('robots'))) ? [] : json_encode($request->input('robots'))),
                'author'=>$request->input('author-seo'),
                'publisher'=>$request->input('publisher-seo'),
            ]);
            $this->entity->analyzer()->create();
            if($request->has('image')){
                $this->entity->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }
            $this->entity->attachTags($request->input('tags'));

            if(!$saved){
                return redirect()->back()->with('error',__('portfolio::portfolios.error'));
            }else{
                return redirect(route("portfolios.index"))->with('message',__('portfolio::portfolios.store'));
            }



        }catch (Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }


    /**
     * Show the form for editing the specified resource.
     * @param $token
     * @return Response
     */
    public function edit($token)
    {
        try {
            $categories=Category::latest()->where('model',Portfolio::class)->get();
            $customers=Customer::latest()->get();
            $item=$this->entity->with('tags')->whereToken($token)->first();
            return view('portfolio::portfolios.edit',compact('item','customers','categories'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param $token
     * @return void
     */
    public function update(PortfolioRequest $request, $token)
    {
        try {

            $customer=Customer::whereToken($request->input('customer'))->firstOrFail();
            $this->entity=$this->entity->whereToken($token)->first();
            $updated=$this->entity->update([
                "user"=>auth('web')->user()->id,
                "title"=>$request->input('title'),
                "slug"=>null,
                "category"=>Category::whereToken($request->input('category'))->first()->id,
                "excerpt"=>$request->input('excerpt'),
                "text"=>$request->input('text'),
                "customer"=>$customer->id,
                "order"=>orderInfo($request->input('order'))
            ]);
            $this->entity->replicate();

            $this->entity->seo()->update([
                'title'=>$request->input('title-seo'),
                'description'=>$request->input('description-seo'),
                'keyword'=>$request->input('keyword-seo'),
                'canonical'=>$request->input('canonical-seo'),
                'robots'=>(is_null(json_encode($request->input('robots'))) ? [] : json_encode($request->input('robots'))),
                'author'=>$request->input('author-seo'),
                'publisher'=>$request->input('publisher-seo'),
            ]);
            if($request->has('image')){
                destroyMedia($this->entity,config('cms.collection-image'));
                $this->entity->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }

            $this->entity->syncTags($request->input('tags'));

            if(!$updated){
                return redirect()->back()->with('error',__('portfolio::portfolios.error'));
            }else{
                return redirect(route("portfolios.index"))->with('message',__('portfolio::portfolios.update'));
            }


        }catch (Exception $exception){

            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param $token
     * @return Response
     */
    public function destroy($token)
    {
        try {
            $this->entity=$this->entity->whereToken($token)->firstOrFail();
            if($this->entity->Hasmedia(config('cms.collection-image'))){
                destroyMedia($this->entity,config('cms.collection-image'));
            }
            $this->entity->seo()->delete();
            $deleted=$this->entity->delete();

            if(!$deleted){
                return redirect()->back()->with('error',__('portfolio::portfolios.error'));
            }else{
                return redirect(route("portfolios.index"))->with('message',__('portfolio::portfolios.delete'));
            }


        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }



}
