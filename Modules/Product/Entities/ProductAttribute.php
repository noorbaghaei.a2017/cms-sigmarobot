<?php

namespace Modules\Product\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Core\Entities\Option;

class ProductAttribute extends Model
{
    protected $table='product_attributes';

    protected $fillable = ['option'];

    public function option_item(){
        return $this->belongsTo(Option::class,'option','id');
    }
    public function attribute_item(){
        return $this->belongsTo(OptionAttribute::class,'attributeable_id','id');
    }
}
