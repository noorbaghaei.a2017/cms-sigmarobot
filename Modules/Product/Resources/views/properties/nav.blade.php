<li>
        <a href="{{route('properties.index')}}" class="b-success">
                              <span class="nav-icon text-white no-fade">
                                <i class="{{config('product.icons.property')}}"> </i>
                              </span>
            <span class="nav-text">{{__('product::properties.collect')}}</span>
        </a>
    </li>
