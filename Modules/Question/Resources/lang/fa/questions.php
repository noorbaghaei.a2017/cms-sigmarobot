<?php

return [
    "text-create"=>' با استفاده از فرم زیر میتوانید سوال جدید اضافه کنید.',
    "text-edit"=>' با استفاده از فرم زیر میتوانید سوال خود را ویرایش کنید.',
    "store"=>"ثبت با موفقیت انجام شد.",
    "delete"=>"حذف با موفقیت انجام شد",
    "update"=>"بروز رسانی با موفقیت انجام شد",
    "index"=>"لیست سوالات متداول",
    "singular"=>"سوال",
    "collect"=>"سوالات متداول",
    "permission"=>[
        "questions-full-access"=>"دسترسی کامل به سوالات",
        "questions-list"=>"لیست سوالات",
        "questions-delete"=>"حذف سوال",
        "questions-create"=>"ایجاد سوال",
        "questions-edit"=>"ویرایش سوال",
    ]
];
