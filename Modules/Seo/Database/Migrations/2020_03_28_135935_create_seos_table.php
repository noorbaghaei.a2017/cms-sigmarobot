<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('keyword')->nullable();
            $table->text('description')->nullable();
            $table->string('title')->nullable();
            $table->json('robots')->nullable();
            $table->text('publisher')->nullable();
            $table->text('author')->nullable();
            $table->text('canonical')->nullable();
            $table->morphs('seoable');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seos');
    }
}
