<?php
return [
    "text-create"=>"you can create your specialdiscount",
    "text-edit"=>"you can edit your specialdiscount",
    "store"=>"Store Success",
    "delete"=>"Delete Success",
    "update"=>"Update Success",
    "index"=>"specialdiscounts list",
    "error"=>"error",
    "singular"=>"specialdiscount",
    "collect"=>"specialdiscounts",
    "permission"=>[
        "specialdiscount-full-access"=>"specialdiscounts full access",
        "specialdiscount-list"=>"specialdiscounts list",
        "specialdiscount-delete"=>"specialdiscount delete",
        "specialdiscount-create"=>"specialdiscount create",
        "specialdiscount-edit"=>"edit specialdiscount",
    ]


];
