<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use Illuminate\Support\Facades\Route;

Route::group(["prefix"=>config('cms.prefix-admin'), "middleware" => ["auth:web"]], function () {
    Route::resource('/specialdiscounts', 'SpecialDiscountController')->only('create','store','destroy','update','index','edit');

    Route::group(["prefix"=>'search'], function () {
        Route::post('/specialdiscounts', 'SpecialDiscountController@search')->name('search.specialdiscount');
    });


});
