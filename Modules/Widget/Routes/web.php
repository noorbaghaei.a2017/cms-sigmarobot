<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::group(["prefix"=>config('cms.prefix-admin'), "middleware" => ["auth:web"]], function () {
    Route::resource('/widgets', 'WidgetController')->only('create','store','destroy','update','index','edit');

    Route::group(["prefix"=>'search'], function () {
        Route::post('/widgets', 'WidgetController@search')->name('search.widget');
    });


    Route::group(["prefix"=>'widget/categories'], function () {
        Route::get('/', 'WidgetController@categories')->name('widget.categories');
        Route::get('/create', 'WidgetController@categoryCreate')->name('widget.category.create');
        Route::post('/store', 'WidgetController@categoryStore')->name('widget.category.store');
        Route::get('/edit/{category}', 'WidgetController@categoryEdit')->name('widget.category.edit');
        Route::patch('/update/{category}', 'WidgetController@categoryUpdate')->name('widget.category.update');

    });

    Route::group(["prefix"=>'widget/questions'], function () {
        Route::get('/{widget}', 'WidgetController@question')->name('widget.questions');
        Route::get('/create/{widget}', 'WidgetController@questionCreate')->name('widget.question.create');
        Route::post('/store/{widget}', 'WidgetController@questionStore')->name('widget.question.store');
        Route::delete('/destroy/{question}', 'WidgetController@questionDestroy')->name('widget.question.destroy');
        Route::get('/edit/{widget}/{question}', 'WidgetController@questionEdit')->name('widget.question.edit');
        Route::patch('/update/{question}', 'WidgetController@questionUpdate')->name('widget.question.update');
    });

});

