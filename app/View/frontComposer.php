<?php

namespace App\View;


use App\Events\Order;
use Illuminate\View\View;
use Modules\Advertising\Entities\Advertising;
use Modules\Advertising\Entities\Education;
use Modules\Advertising\Entities\Experience;
use Modules\Advertising\Entities\Guild;
use Modules\Advertising\Entities\Position;
use Modules\Advertising\Entities\Salary;
use Modules\Advertising\Entities\Skill;
use Modules\Advertising\Entities\TypeAdvertising;
use Modules\Article\Entities\Article;
use Modules\Brand\Entities\Brand;
use Modules\Carousel\Entities\Carousel;
use Modules\Chat\Entities\Chat;
use Modules\Client\Entities\Client;
use Modules\Core\Entities\Area;
use Modules\Core\Entities\Category;
use Modules\Core\Entities\City;
use Modules\Core\Entities\Contact;
use Modules\Core\Entities\Country;
use Modules\Core\Entities\Currency;
use Modules\Core\Entities\Setting;
use Modules\Core\Entities\State;
use Modules\Customer\Entities\Customer;
use Modules\Download\Entities\Download;
use Modules\Educational\Entities\ClassRoom;
use Modules\Educational\Entities\Race;
use Modules\Event\Entities\Event;
use Modules\Information\Entities\Information;
use Modules\Member\Entities\Member;
use Modules\Menu\Entities\ListMenu;
use Modules\Menu\Entities\Menu;
use Modules\Payment\Entities\Payment;
use Modules\Plan\Entities\Plan;
use Modules\Portfolio\Entities\Portfolio;
use Modules\Product\Entities\Product;
use Modules\Question\Entities\Question;
use Modules\Service\Entities\Advantage;
use Modules\Service\Entities\Property;
use Modules\Service\Entities\Service;
use Modules\Store\Entities\Store;

class frontComposer{


    public function compose(View $view){

        $download=Download::orderBy('order','asc')->get();
        if(auth('client')->check()){
            $view->with('client',Client::with('info','myCode','classrooms','races','adverts','plan','sendCV','wallet','cart')->find(\auth('client')->user()->id));
            $view->with('chats',Chat::where('client',\auth('client')->user()->id)->get());

        }

        $view->with('setting',Setting::with('info')->first());
        $view->with('plans', Plan::orderBy('order','asc')->whereStatus(1)->get());
        $view->with('companies', Client::with('company','info')->latest()
            ->whereHas('role',function ($query){
                $query->where('title', '=', 'employer');
            })->get()
        );
        $view->with('all_category_advertisings', Category::with('advertisings')->orderBy('order','asc')->where('model',Advertising::class)->whereParent(0)->get());
        $view->with('all_category_products', Category::with('products')->orderBy('order','asc')->where('model',Product::class)->whereParent(0)->get());
        $view->with('category_advertisings', Category::with('advertisings')->orderBy('order','asc')->where('model',Advertising::class)->whereParent(0)->take(4)->get());
        $view->with('category_companies', Category::orderBy('order','asc')->where('model',Client::class)->whereParent(0)->get());
        $view->with('tehran_areas', Area::where('city',City::where('name','Tehran')->first()->id)->get());
        $view->with('countries', Country::orderBy('name','desc')->get());
        $view->with('currencies', Currency::orderBy('created_at','desc')->get());
        $view->with('positions', Position::orderBy('created_at','desc')->get());
        $view->with('experiences', Experience::orderBy('created_at','desc')->get());
        $view->with('experiences', Experience::orderBy('created_at','desc')->get());
        $view->with('guilds', Guild::orderBy('created_at','desc')->get());
        $view->with('kinds', TypeAdvertising::orderBy('created_at','desc')->get());
        $view->with('salaries', Salary::orderBy('created_at','desc')->get());
        $view->with('skills', Skill::orderBy('created_at','desc')->get());
        $view->with('educations', Education::orderBy('created_at','desc')->get());
        $view->with('states', State::orderBy('name','desc')->get());
        $view->with('cities', City::orderBy('name','desc')->get());
        $view->with('all_category_advertisings', Category::with('advertisings')->orderBy('order','asc')->where('model',Advertising::class)->whereParent(0)->get());
        $view->with('all_category_portfolios', Category::with('advertisings')->orderBy('order','asc')->where('model',Portfolio::class)->whereParent(0)->get());
        $view->with('all_plans_advertisings', Plan::with('price')->orderBy('order','asc')->whereStatus(1)->get());
        $view->with('plans_advertisings', Plan::with('price')->orderBy('order','asc')->whereStatus(1)->take(3)->get());
        $view->with('original_category_products', Category::with('advertisings')->orderBy('order','asc')->where('model',Product::class)->whereParent(0)->get());
        $view->with('advertisings', Advertising::where('status',1)->get());
        $view->with('members', Member::orderBy('order','desc')->get());
        $view->with('courses', ClassRoom::orderBy('order','desc')->get());
        $view->with('classroomscount', ClassRoom::latest()->count());
        $view->with('racescount', Race::latest()->count());
        $view->with('advertisingscount', Advertising::latest()->count());
        $view->with('professorscount', Member::latest()
            ->whereHas('role',function ($query){
                $query->where('title', '=', 'professor');
            })
            ->count());
        $view->with('seekerscount', Client::latest()
            ->whereHas('role',function ($query){
                $query->where('title', '=', 'seeker');
            })
            ->count());
        $view->with('employerscount', Client::latest()
            ->whereHas('role',function ($query){
                $query->where('title', '=', 'employer');
            })
            ->count());
        $view->with('clientscount', Client::latest()
            ->whereHas('role',function ($query){
                $query->where('title', '=', 'user');
            })
            ->count());
        $view->with('popular_companies', Client::with('company')
            ->whereHas('role',function ($query){
                $query->where('title', '=', 'employer');
            })->get()->sortByDESC(function($query){
                return $query->analyzer->view;
            })->take(4));
        $view->with('popular_courses', ClassRoom::get()->sortByDESC(function($query){
            return $query->analyzer->view;
        })->take(4));

        $view->with('popular_articles', Article::get()->sortByDESC(function($query){
            return $query->analyzer->view;
        })->take(4));
        $view->with('popular_products', Product::get()->sortByDESC(function($query){
            return $query->analyzer->view;
        })->take(4));
        $view->with('popular_races', Race::get()->sortByDESC(function($query){
            return $query->analyzer->view;
        })->take(4));
        $view->with('professors', Member::orderBy('order','asc')->whereStatus(1)->with('professor')->has('professor')->get());
        $view->with('except_professors', Member::orderBy('order','asc')->where('role','!=',15)->whereStatus(1)->get());
        $view->with('races', Race::orderBy('order','desc')->get());
        $view->with('listmenus', ListMenu::latest()->get());
        $view->with('top_menus', Menu::orderBy('order','asc')->where('parent',0)->where('list_menus',ListMenu::where('name','top-menu')->first()->id)->get());
        $view->with('bottom_menus', Menu::orderBy('order','asc')->where('parent',0)->where('list_menus',ListMenu::where('name','bottom-menu')->first()->id)->get());
        $view->with('customers', Customer::orderBy('order','asc')->get());
        $view->with('sliders', Carousel::orderBy('order','asc')->get());
        $view->with('questions', Question::orderBy('order','asc')->get());
        $view->with('contact_count', Contact::latest()->where('seen',0)->count());
        $view->with('events', Event::orderBy('order','asc')->get());
        $view->with('stores', Store::orderBy('order','asc')->get());
        $view->with('brands', Brand::orderBy('order','asc')->get());
        $view->with('downloads', $download->groupBy('category'));
        $view->with('products', Product::with('attributes','properties')->take(6)->get());
        $view->with('last_courses', ClassRoom::take(3)->whereParent(0)->get());
        $view->with('last_races', Race::take(3)->whereParent(0)->get());
        $view->with('last_articles', Article::with('user_info')->take(3)->get());
        $view->with('last_informations', Information::with('user_info')->take(3)->get());
        $view->with('new_products', Product::with('attributes','properties')->whereStatus(1)->take(4)->get());
        $view->with('coming_products', Product::with('attributes','properties')->whereStatus(3)->take(4)->get());
        $view->with('special_products', Product::with('attributes','properties')->whereSpecial(2)->take(4)->get());
        $view->with('new_articles', Article::with('user_info')->orderBy('created_at','desc')->take(3)->get());
        $view->with('new_informations', Information::latest()->take(5)->get());
        $view->with('articles', Article::with('user_info')->orderBy('created_at','desc')->take(6)->get());
        $view->with('portfolios', Portfolio::orderBy('created_at','desc')->take(6)->get());
        $view->with('informations', Information::with('user_info')->orderBy('created_at','desc')->take(6)->get());
        $view->with('members', Member::orderBy('created_at','desc')->take(6)->get());
        $view->with('skills', Skill::orderBy('created_at','desc')->get());
        $view->with('salaries', Salary::orderBy('created_at','desc')->get());
        $view->with('services', Service::orderBy('created_at','desc')->whereParent(0)->get());
        $view->with('properties', Property::all());
        $view->with('advantages', Advantage::all());

    }

}
