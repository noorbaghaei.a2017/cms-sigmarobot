@extends('template.app')

@section('content')

    <!-- Start Blog Area -->
    <section class="dcare__blog__area section-padding--lg bg--white" style="padding-top: 30px !important;">
        <div class="container">
            <div class="row blog-page" style="justify-content: flex-end;">
            @foreach($items as $item)
                <!-- Start Single Blog -->
                    <div class="col-lg-4 col-md-6 col-sm-12">
                        <div class="blog__2">
                            <div class="blog__thumb">
                                <a href="{{route('articles.single',['article'=>$item->slug])}}">
                                    @if(!$item->Hasmedia('images'))
                                        <img src="{{asset('template/images/blog/bl-2/1.jpg')}}" alt="{{$item->title}}" title="{{$item->title}}">
                                    @else

                                        <img src="{{$item->getFirstMediaUrl('images')}}" alt="{{$item->title}}" title="{{$item->title}}" >
                                    @endif
                                </a>
                            </div>
                            <div class="blog__inner text-right">
                                <div class="blog__hover__inner">
                                    <h2><a href="{{route('articles.single',['article'=>$item->slug])}}">{{$item->title}}</a></h2>
                                    <div class="bl__meta">
                                        <p>{{Morilog\Jalali\Jalalian::forge($item->created_at)->format('%A, %d %B %y')}}</p>
                                    </div>
                                    <div class="bl__details">
                                        <p>{{$item->excerpt}}</p>
                                    </div>
                                    <div class="blog__btn">
                                        <a class="bl__btn" href="{{route('articles.single',['article'=>$item->slug])}}">ادامه مطلب</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Single Blog -->
                @endforeach

            </div>

        </div>
    </section>
    <!-- End Blog Area -->




@endsection



