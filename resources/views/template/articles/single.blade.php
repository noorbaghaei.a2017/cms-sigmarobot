@extends('template.app')

@section('content')

    <!-- Start Blog Grid Area -->
    <div class="dcare__blog__list bg--white section-padding--lg" style="padding-top: 30px !important;">
        <div class="container">
            <div class="row">
                <!-- Start BLog Details -->
                <div class="col-lg-9 text-right" style="direction:rtl">
                    <div class="page__blog__details">
                        <article class="dacre__blog__details">
                            <div class="blog__thumb">
                                @if(!$item->Hasmedia('images'))
                                    <img src="{{asset('template/images/blog/big-img/6.jpg')}}" alt="logo images">
                                @else

                                    <img src="{{$item->getFirstMediaUrl('images')}}" alt="blog images" >
                                @endif

                            </div>
                            <div class="blog__inner text-right">
                                <h2>{{$item->title}}</h2>
                                <ul>
                                    <li>{{Morilog\Jalali\Jalalian::forge($item->created_at)->format('%A, %d %B %y')}}</li>
                                    <li><a href="#">بازدید : {{$item->view}}</a></li>
                                </ul>

    {!! $item->text !!}


                            </div>
                        </article>




                        <div class="blog__page__btn">

                            <div class="blog__share">
                                <span>اشتراک:</span>
                                <ul class="dacre__social__link--2 bg--2 d-flex justify-content-end">
                                    <li class="twitter"><a target="_blank" href='https://telegram.org/js/telegram-widget.js?14"data-telegram-share-url="{{\Request::url()}}"'><i class="fa fa-telegram"></i></a></li>
                                    <li class="vimeo"><a target="_blank" href="whatsapp://send?text={{\Request::url()}}"><i class="fa fa-whatsapp"></i></a></li>
                                </ul>
                            </div>
                        </div>


                        <!-- Start Latest Blog -->
                        <div class="blog__latest">
                            <h4 class="title__line--3"> آخرین مقالات</h4>
                            <div class="row">

                                @foreach($last_articles as $last_article)
                                <!-- Start Single BLog -->
                                <div class="col-12 col-lg-6 col-md-6">
                                    <article class="blog__single blog__item">
                                        <div class="blog__thumb">
                                            <a href="{{route('articles.single',['article'=>$last_article->slug])}}">
                                                @if(!$last_article->Hasmedia('images'))
                                                    <img src="{{asset('template/images/blog/big-img/10.jpg')}}" alt="logo images" width="370" height="215">
                                                @else

                                                    <img src="{{$last_article->getFirstMediaUrl('images')}}" alt="logo images" width="370" height="215">
                                                @endif
                                            </a>
                                        </div>
                                        <div class="blog__content">
                                            <h2><a href="{{route('articles.single',['article'=>$last_article->slug])}}">{{$last_article->title}}</a></h2>
                                            <ul class="bl__post" style="padding-right: 10px">
                                                <li>تاریخ : {{Morilog\Jalali\Jalalian::forge($item->created_at)->format('%A, %d %B %y')}}</li>
                                            </ul>
                                           <p>
                                               {{$last_article->excerpt}}
                                           </p>
                                            <div class="blog__btn">
                                                <a class="dcare__btn btn__f6f6f6" href="{{route('articles.single',['article'=>$last_article->slug])}}">ادامه مطلب</a>

                                            </div>
                                        </div>
                                    </article>
                                </div>
                                <!-- End Single BLog -->
                                    @endforeach



                            </div>
                        </div>
                        <!-- End Latest Blog -->


                    </div>
                </div>
                <!-- End BLog Details -->
                <!-- Start Sidebar -->
                <div class="col-lg-3 text-right" style="direction: rtl">
                    <div class="sidebar__widgets sidebar--right">



                        <!-- Single Widget -->
                        <div class="single__widget recent__post">
                            <h4>آخرین مقالات</h4>
                            <ul>
                                @foreach($last_articles as $last_article)
                                <li>
                                    <a href="{{route('articles.single',['article'=>$last_article->slug])}}"><img src="{{asset('template/images/blog/sm-img/1.jpg')}}" alt="post images"></a>
                                    <div class="post__content">
                                        <h6><a href="{{route('articles.single',['article'=>$last_article->slug])}}">{{$last_article->title}}</a></h6>
                                        <span class="date"><i class="fa fa-calendar"></i>{{Morilog\Jalali\Jalalian::forge($last_article->created_at)->format('%A, %d %B %y')}}</span>
                                    </div>
                                </li>
                                @endforeach

                            </ul>
                        </div>
                        <!-- End Widget -->





                        <!-- Single Widget -->
                        <div class="single__widget Popular__classes">
                            <h4>مقاله های پربازدید</h4>
                            <ul>
                                @foreach($last_articles as $last_article)
                                <li><a href="{{route('articles.single',['article'=>$last_article->slug])}}">{{$last_article->title}}</a></li>

                                @endforeach
                            </ul>
                        </div>
                        <!-- End Widget -->

                        <!-- Single Widget -->
                        <div class="single__widget tags text-right">
                            <h4>برچسب ها</h4>
                            <ul style="justify-content: flex-start">
                                @foreach($item->tags as $tag)
                                    <li><a href="#">{{$tag->name}}</a></li>
                                @endforeach
                            </ul>
                        </div>
                        <!-- End Widget -->



                    </div>
                </div>
                <!-- End Sidebar -->
            </div>
        </div>
    </div>
    <!-- End Blog Grid Area -->


@endsection

@section('head')

    <meta name="csrf-token" content="{{ csrf_token() }}">

@endsection


@section('script')


    <script>
        $("a.ad").bind("click", function () {
            $code=$(this).data('param');
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type:'POST',
                contentType:'application/json; charset=utf-8',
                url:'/ajax/count/ad/'+$code,
                data: { field1:$code} ,
                success: function (response) {
                },
                error: function (response) {

                },
            });
        });


    </script>

@endsection

