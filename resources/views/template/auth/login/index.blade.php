@extends('template.auth.app')

@section('content')
<div class="limiter">
    <div class="container-login100">
        <div class="wrap-login100 text-right" style="direction: rtl">
            <div class="login100-pic js-tilt" data-tilt>
                <img src="{{asset('template/images/auth/register-pic.jpg')}}" alt="IMG">
            </div>

            <form action="{{ route('client.login.submit') }}" method="POST" class="login100-form validate-form">
                    @csrf
                <div class="text-center">
                    @if(!$setting->Hasmedia('logo'))

                        <a href="{{route('front.website')}}">
                            <img src="{{asset('img/no-img.gif')}}" alt="{{$setting->name}}" title="{{$setting->name}}" width="100">

                        </a>
                    @else
                        <a href="{{route('front.website')}}">
                             <img src="{{$setting->getFirstMediaUrl('logo')}}" alt="{{$setting->name}}" title="{{$setting->name}}" width="100">

                        </a>
                    @endif
                </div>
					<span class="login100-form-title">
						ورود
					</span>

                <div class="wrap-input100" >
                    @error('email')
                    <span class="invalid-feedback" role="alert" style="display: block">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                    @error('mobile')
                    <span class="invalid-feedback" role="alert" style="display: block">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                    @error('username')
                    <span class="invalid-feedback" role="alert" style="display: block">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                    <input class="input100" type="text" name="identify" data-validate = "نام کاربری ، ایمیل یا موبایل الزامی است" placeholder="نام کاربری ، ایمیل یا موبایل">
                    <span class="focus-input100"></span>
                    <span class="symbol-input100">
							<i class="fa fa-phone" aria-hidden="true" style="margin-right: 10px"></i>
						</span>
                </div>

                <div class="wrap-input100 validate-input" data-validate = "پسورد الزامی است">
                    @error('password')
                    <span class="invalid-feedback" role="alert" style="display: block">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                    <input class="input100" type="password" name="password" placeholder="کلمه عبور">
                    <span class="focus-input100"></span>
                    <span class="symbol-input100">
							<i class="fa fa-lock" aria-hidden="true" style="margin-right: 10px"></i>
						</span>
                </div>

                <div class="container-login100-form-btn">
                    <button type="submit" class="login100-form-btn">
                        ورود
                    </button>
                </div>


                <div class="text-center p-t-136">
                    <a class="txt2" href="{{route('client.register')}}">
                       ثبت نام
                        <i class="fa fa-long-arrow-right m-l-5" aria-hidden="true"></i>
                    </a>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

