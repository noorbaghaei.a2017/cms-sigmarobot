@extends('template.auth.app')

@section('content')
<div class="limiter">
    <div class="container-login100">
        <div class="wrap-login100 text-right" style="direction: rtl">
            <div class="login100-pic js-tilt" data-tilt>
                <img src="{{asset('template/images/auth/register-pic.jpg')}}" alt="IMG">
            </div>

            <form action="{{route('client.register.submit.client')}}" method="POST" class="login100-form validate-form">
                    @csrf
                <div class="text-center">
                    @if(!$setting->Hasmedia('logo'))

                        <a href="{{route('front.website')}}">
                            <img src="{{asset('img/no-img.gif')}}" alt="{{$setting->name}}" title="{{$setting->name}}" width="100">

                        </a>
                    @else
                        <a href="{{route('front.website')}}">
                            <img src="{{$setting->getFirstMediaUrl('logo')}}" alt="{{$setting->name}}" title="{{$setting->name}}" width="100">

                        </a>
                    @endif
                </div>
					<span class="login100-form-title">
						ثبت نام
					</span>

                <div class="wrap-input100" >

                    @error('first_name')
                    <span class="invalid-feedback" role="alert" style="display: block">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                    <input class="input100" type="text" name="first_name" value="{{old('first_name')}}" data-validate = "نام" placeholder=" نام">
                    <span class="focus-input100"></span>
                    <span class="symbol-input100">
							<i class="fa fa-user" aria-hidden="true" style="margin-right: 10px"></i>
						</span>
                </div>
                <div class="wrap-input100" >
                    @error('last_name')
                    <span class="invalid-feedback" role="alert" style="display: block">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                    <input class="input100" type="text" name="last_name" value="{{old('last_name')}}" data-validate = "نام خانوادگی" placeholder=" نام خانوادگی">
                    <span class="focus-input100"></span>
                    <span class="symbol-input100">
							<i class="fa fa-users" aria-hidden="true" style="margin-right: 10px"></i>
						</span>
                </div>
                <div class="wrap-input100" >

                    @error('mobile')
                    <span class="invalid-feedback" role="alert"  style="display: block">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                    <input class="input100" type="text" name="mobile" value="{{old('mobile')}}" data-validate = "موبایل" placeholder=" موبایل">
                    <span class="focus-input100"></span>
                    <span class="symbol-input100">
							<i class="fa fa-phone" aria-hidden="true" style="margin-right: 10px"></i>
						</span>
                </div>
                <div class="wrap-input100" >

                    @error('password')
                    <span class="invalid-feedback" role="alert" style="display: block">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                    <input class="input100" type="text" name="password" data-validate = "کلمه عبور" placeholder=" کلمه عبور">
                    <span class="focus-input100"></span>
                    <span class="symbol-input100">
							<i class="fa fa-lock" aria-hidden="true" style="margin-right: 10px"></i>
						</span>


                </div>


                <div class="container-login100-form-btn">
                    <button type="submit" class="login100-form-btn">
                        ثبت نام
                    </button>
                </div>

            </form>
        </div>
    </div>
</div>
@endsection

