@extends('template.app')

@section('content')
<div class="hidden-text">

    <h1>آموزش رباتیک</h1>
    <h1>رباتیک دانش آموزی</h1>
</div>
    <!-- Start Courses Area -->
    <section class="dcare__courses__area section-padding--lg bg--white" style="padding-top: 15px !important;">
        <div class="container">
            <div class="row class__grid__page" style="direction: rtl">
                <div>
                    @include('core::layout.alert-success')
                    @include('core::layout.alert-danger')
                </div>
                <!-- Start Single Courses -->
                @foreach($items as $item)
                <div class="col-lg-3 col-md-5 col-sm-6 col-6">
                    <div class="courses">
                        <div class="courses__thumb">
                            <a href="{{route('courses.single',['course'=>$item->slug])}}">
                                @if(!$item->Hasmedia('images'))
                                    <img src="{{asset('img/no-img.gif')}}" alt="courses images" width="370" height="170">
                                @else

                                    <img src="{{$item->getFirstMediaUrl('images')}}" alt="courses images" width="370" height="170">
                                @endif
                            </a>
                        </div>
                        <div class="courses__inner">
                            <div class="courses__wrap text-right">
                                <div class="courses__date"><i class="fa fa-calendar"></i>{{Morilog\Jalali\Jalalian::forge($item->start_at)->format('%A, %d %B %y')}}</div>
                                <div class="courses__content">
                                    <h4><a href="{{route('courses.single',['course'=>$item->slug])}}">{{$item->title}}</a></h4>
                                   @if($item->capacity!=0)
                                    <span>ظرفیت : {{$item->capacity}} نفر</span>
                                    @else
                                    <span>  ظرفیت : نامحدود </span>
                                    @endif
                                    <br>
                                    @if($item->price->amount==0)
                                        <span>قیمت : <span style="color: {{$setting->color_theme}}">رایگان</span></span>
                                    @else
                                        <span>  قیمت : <span style="color: {{$setting->color_theme}}">{{number_format($item->price->amount)}}</span> تومان </span>
                                    @endif
                                    <p>
                                        {{  \Illuminate\Support\Str::limit(strip_tags($item->excerpt), 60) }}
                                    </p>
                                    <ul class="rating d-flex">
                                        <li><span class="ti-star"></span></li>
                                        <li><span class="ti-star"></span></li>
                                        <li><span class="ti-star"></span></li>
                                        <li><span class="ti-star"></span></li>
                                        <li><span class="ti-star"></span></li>
                                    </ul>
                                    <div>
                                        @if($item->status==1)
                                        @if(auth('client')->check())

                                            @if(hasAction($item,auth('client')->user(),'classroom'))

                                                <span class="btn btn-block btn-primary">شما عضو این دوره شدید</span>

                                            @else

                                                @if($item->price->amount > 0)
                                                    <form action="{{route('send.request.transaction',['item'=>$item->token,'name'=>'classRoom'])}}" method="POST">
                                                        @csrf


                                                            <button type="submit" class="btn btn-block btn-success">پرداخت شهریه و ثبت نام</button>



                                                    </form>

                                                @elseif($item->price->amount==0)
                                                    @if($item->status==1)
                                                        <a href="{{route('register.registerClassroom',['classroom'=>$item->token])}}" class="btn btn-block btn-success">ثبت نام</a>
                                                    @endif
                                                @endif



                                            @endif


                                        @else

                                            <a href="{{route('client.dashboard')}}" class="btn btn-block btn-warning">وارد سایت شوید</a>

                                        @endif
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
                <!-- End Single Courses -->
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="dcare__pagination mt--80">
{{--                        {!! $items->links() !!}--}}
                    </div>
                </div>
            </div>
        </div>
    </section>



@endsection



