@extends('template.app')

@section('content')

    <!-- Start Our Event Area -->
    <div class="dcare__event__area bg--white section-padding--lg" style="padding-top: 15px !important;">
        <div class="container">
            <div class="row event-grid-page" style="justify-content: flex-end">
                @foreach($items as $item)
                <!-- Start Single Event -->
                <div class="col-lg-4 col-md-6 col-sm-12">
                    <div class="dcare__event">
                        <div class="event__thumb">
                            <a href="{{route('events.single',['event'=>$item->slug])}}">
                                @if(!$item->Hasmedia('images'))
                                    <img src="{{asset('template/images/blog/bl-2/1.jpg')}}" alt="{{$item->title}}" title="{{$item->title}}">
                                @else

                                    <img src="{{$item->getFirstMediaUrl('images')}}" alt="{{$item->title}}" title="{{$item->title}}" >
                                @endif
                            </a>
                        </div>
                        <div class="event__content">
                            <div class="event__pub">
                                <div class="event__date">
                                    <span class="date">{{Morilog\Jalali\Jalalian::forge($item->start_at)->format(' %d')}}</span><span>{{Morilog\Jalali\Jalalian::forge($item->start_at)->format('%B')}}</span>
                                </div>
                                <ul class="event__time">
                                    <li> <i class="fa fa-clock-o"></i>{{Morilog\Jalali\Jalalian::forge($item->start_at)->format('%A, %d %B %y')}}</li>
                                    <li><i class="fa fa-home"></i>{{showCategory($item->category)->symbol}}</li>
                                </ul>
                            </div>
                            <div class="event__inner">
                                <p style="direction: rtl"><a href="{{route('events.single',['event'=>$item->slug])}}" style="font-size: 20px">{{  \Illuminate\Support\Str::limit(strip_tags($item->excerpt), 30) }}</a></p>
                            </div>
                            <ul class="event__btn">
                                <li><a href="{{route('events.single',['event'=>$item->slug])}}">ادامه مطلب</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- Start Single Event -->
{{--                <!-- Start Single Event -->--}}
{{--                <div class="col-lg-4 col-md-6 col-sm-12">--}}
{{--                    <div class="dcare__event">--}}
{{--                        <div class="event__content">--}}
{{--                            <div class="event__pub">--}}
{{--                                <div class="event__date">--}}
{{--                                    <span class="date">14</span><span>th</span>--}}
{{--                                </div>--}}
{{--                                <ul class="event__time">--}}
{{--                                    <li>December, <i class="fa fa-clock-o"></i>5.00 am to 9.00 pm</li>--}}
{{--                                    <li><i class="fa fa-home"></i>Childrens Club, Uttara, Dhaka</li>--}}
{{--                                </ul>--}}
{{--                            </div>--}}
{{--                            <div class="event__inner">--}}
{{--                                <p><a href="#">Holiday ! Toy Making Workshop Wakw Up...</a></p>--}}
{{--                            </div>--}}
{{--                            <ul class="event__btn">--}}
{{--                                <li><a href="#">Book Now</a></li>--}}
{{--                                <li><a href="#">Learn More</a></li>--}}
{{--                            </ul>--}}
{{--                        </div>--}}
{{--                        <div class="event__thumb">--}}
{{--                            <a href="#">--}}
{{--                                <img src="{{asset('template/images/event/mid-img-2/2.jpg')}}" alt="event images">--}}
{{--                            </a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <!-- Start Single Event -->--}}
                    @endforeach

            </div>
{{--            <div class="row">--}}
{{--                <div class="col-lg-12">--}}
{{--                    <div class="dcare__pagination mt--80">--}}
{{--                        <ul class="dcare__page__list d-flex justify-content-center">--}}
{{--                            <li><a href="#"><span class="ti-angle-double-left"></span></a></li>--}}
{{--                            <li><a class="page" href="#">Prev</a></li>--}}
{{--                            <li><a href="#">1</a></li>--}}
{{--                            <li><a href="#">2</a></li>--}}
{{--                            <li><a href="#"><i class="fa fa-ellipsis-h"></i></a></li>--}}
{{--                            <li><a href="#">28</a></li>--}}
{{--                            <li><a class="page" href="#">Next</a></li>--}}
{{--                            <li><a href="#"><span class="ti-angle-double-right"></span></a></li>--}}
{{--                        </ul>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
        </div>
    </div>
    <!-- End Our Event Area -->





@endsection



