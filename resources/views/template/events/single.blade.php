@extends('template.app')

@section('content')

    <!-- Start Class Details -->
    <section class="page-event-details bg--white section-padding--lg" style="padding-top: 15px !important;">
        <div class="container">
            <div class="row">
                <div class="col-lg-9">
                    <div class="event-content-wrapper text-right">

                        <div class="event-section" style="direction: rtl">


                            <div class="event__inner">
                                <span><i class="fa fa-calendar"></i>{{Morilog\Jalali\Jalalian::forge($item->start_at)->format('%A, %d %B %y')}}</span>
                                <h4>{{$item->title}}</h4>

                                {{$item->excerpt}}

                            </div>
                            <div class="enent__thumb">

                                @if(!$item->Hasmedia('images'))
                                    <img src="{{asset('template/images/blog/bl-2/1.jpg')}}" alt="{{$item->title}}" title="{{$item->title}}">
                                @else

                                    <img src="{{$item->getFirstMediaUrl('images')}}" alt="{{$item->title}}" title="{{$item->title}}" >
                                @endif
{{--                                <div class="box-timer">--}}
{{--                                    <div class="countbox timer-grid">--}}
{{--                                        <div  data-countdown="{{countDown($item->start_at)}}"></div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
                            </div>
                            {!! $item->text !!}

                        </div>


                        <div class="event__share d-flex">
                            <span>اشتراک :</span>
                            <ul class="dacre__social__link--3 social-net-3 d-flex justify-content-start">
                                <li class="facebook"><a target="_blank"  href='https://telegram.org/js/telegram-widget.js?14"data-telegram-share-url="{{\Request::url()}}"'><i class="fa fa-telegram"></i></a></li>
                                <li class="vimeo"><a target="_blank" href="whatsapp://send?text={{\Request::url()}}"><i class="fa fa-whatsapp"></i></a></li>
                            </ul>
                        </div>

                        <div class="upcoming__event mt--80">
                            <h2>رویداد های مرتبط</h2>
                            <div class="row">
{{--                                <div class="col-lg-6">--}}
{{--                                    <!-- Start Event -->--}}
{{--                                    <div class="single__event d-flex">--}}
{{--                                        <div class="event__thumb">--}}
{{--                                            <a href="event-details.html">--}}
{{--                                                <img src="images/event/sm-img/1.jpg" alt="event images">--}}
{{--                                            </a>--}}
{{--                                            <div class="event__hover__info">--}}
{{--                                                <span>16th Dec</span>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                        <div class="event__inner">--}}
{{--                                            <h6><a href="event-details.html">Parents Day</a></h6>--}}
{{--                                            <ul class="event__time__location">--}}
{{--                                                <li><i class="fa fa-home"></i>Childrens Club, Uttara, Dhaka</li>--}}
{{--                                                <li><i class="fa fa-clock-o"></i>5.00 am to 9.00 pm</li>--}}
{{--                                            </ul>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <!-- End Event -->--}}
{{--                                </div>--}}

                            </div>
                        </div>

                    </div>
                </div>
                <!-- Start Sidebar -->
                <div class="col-lg-3">
                    <div class="sidebar__widgets sidebar--right text-right">



                        <!-- Single Widget -->
                        <div class="single__widget offer">
                            <div class="about__content">
                                <a class="ad" href="{{getAd('jlIRadWgPQFuPKvPuiC2Gxmy5EV9ett1y0G')->href}}" target="_blank" data-param="jlIRadWgPQFuPKvPuiC2Gxmy5EV9ett1y0G">
                                    <img src="{{getAd('jlIRadWgPQFuPKvPuiC2Gxmy5EV9ett1y0G')->getFirstMediaUrl('images')}}" alt="">
                                </a>
                                <div class="about__info">
                                    <div class="about__inner">

                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Widget -->

                        <!-- Single Widget -->
                        <div class="single__widget recent__post">
                            <h4>آخرین رویداد ها</h4>
                            <ul>
                                @foreach($events as $event)
                                <li>
                                    <a href="{{route('events.single',['event'=>$event->slug])}}">
                                        @if(!$event->Hasmedia('images'))
                                            <img src="{{asset('template/images/blog/bl-2/1.jpg')}}" alt="{{$event->title}}" title="{{$event->title}}">
                                        @else

                                            <img src="{{$event->getFirstMediaUrl('images')}}" alt="{{$event->title}}" title="{{$event->title}}" >
                                        @endif
                                    </a>
                                    <div class="post__content">
                                        <h6><a  href="{{route('events.single',['event'=>$event->slug])}}">{{$event->title}}</a></h6>
                                        <span class="date"><i class="fa fa-calendar"></i>{{Morilog\Jalali\Jalalian::forge($event->start_at)->format('%A, %d %B %y')}}</span>
                                    </div>
                                </li>
                                @endforeach


                            </ul>
                        </div>
                        <!-- End Widget -->

                        <!-- Single Widget -->
                        <div class="single__widget offer">
                            <div class="offer__thumb">
                                <a class="ad" href="{{getAd('SzvGz12ZgWHJC6sTxxWywmUNuuFkP2pUsnG')->href}}" target="_blank" data-param="SzvGz12ZgWHJC6sTxxWywmUNuuFkP2pUsnG">
                                    <img src="{{getAd('SzvGz12ZgWHJC6sTxxWywmUNuuFkP2pUsnG')->getFirstMediaUrl('images')}}" alt="">
                                </a>
                            </div>
                        </div>
                        <!-- End Widget -->



                        <!-- Single Widget -->
                        <div class="single__widget tags">
                            <h4>برچسب ها</h4>
                            <ul style="justify-content: flex-end">
                                @foreach($item->tags as $tag)
                                <li><a href="#">{{$tag->name}}</a></li>
                                @endforeach
                            </ul>
                        </div>
                        <!-- End Widget -->

                    </div>
                </div>
                <!-- End Sidebar -->
            </div>
        </div>
    </section>
    <!-- End Class Details -->
    <!-- Start Subscribe Area -->


@endsection
@section('head')

    <meta name="csrf-token" content="{{ csrf_token() }}">

@endsection

@section('script')


    <script>
        $("a.ad").bind("click", function () {
            $code=$(this).data('param');
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type:'POST',
                contentType:'application/json; charset=utf-8',
                url:'/ajax/count/ad/'+$code,
                data: { field1:$code} ,
                success: function (response) {
                },
                error: function (response) {

                },
            });
        });


    </script>

@endsection

