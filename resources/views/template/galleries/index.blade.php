@extends('template.app')

@section('content')
    <div class="hidden-text">

        <h1>آموزش رباتیک</h1>
        <h1>رباتیک دانش آموزی</h1>
    </div>
    <!-- Start Our Gallery Area -->
    <div class="junior__gallery__area gallery-page-one gallery__masonry__activation gallery--3 bg-image--25 section-padding--lg" style="padding-top: 15px !important;">
        <div class="container">

            <div class="row galler__wrap masonry__wrap mt--80">
                @foreach($categories as $category)
                <!-- Start Single Gallery -->
                <div class="col-lg-3 col-md-4 col-sm-6 col-6 gallery__item gallery--{{$category->id}}" >
                    <div class="gallery" style="padding: 0 5px">
                        <div class="gallery__thumb" style="margin-top: 30px;display: inline-block">
                            <a href="{{route('galleries.single',['category'=>$category->id])}}">
                                @if(!$category->Hasmedia('images'))
                                    <img src="{{asset('img/no-img.gif')}}" alt="{{$category->symbol}}" title="{{$category->symbol}}">
                                @else
                                    <img src="{{$category->getFirstMediaUrl('images')}}" alt="{{$category->symbol}}" title="{{$category->symbol}}">
                                @endif
                            </a>
                        </div>
                        <div class="gallery__hover__inner">
                            <div class="gallery__hover__action">
                                <ul class="gallery__zoom">
                                    @if(!$category->Hasmedia('images'))
                                        <li><a href="{{asset('img/no-img.gif')}}" data-lightbox="grportimg" data-title="{{$category->symbol}}"><i class="fa fa-search"></i></a></li>
                                    @else
                                        <li><a href="{{$category->getFirstMediaUrl('images')}}" data-lightbox="grportimg" data-title="{{$category->symbol}}"><i class="fa fa-search"></i></a></li>
                                    @endif



                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="footer-gallery">
                        <a href="{{route('galleries.single',['category'=>$category->id])}}">
                            <h6 >{{$category->symbol}}</h6>
                        </a>
                    </div>
                </div>
                <!-- End Single Gallery -->
                    @endforeach


            </div>
        </div>
    </div>
    <!-- End Our Gallery Area -->


@endsection



