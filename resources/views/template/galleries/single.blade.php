@extends('template.app')

@section('content')
    <div class="hidden-text">

        <h1>آموزش رباتیک</h1>
        <h1>رباتیک دانش آموزی</h1>
    </div>
    <!-- Start Our Gallery Area -->
    <div class="junior__gallery__area gallery-page-one gallery__masonry__activation gallery--3 bg-image--25 section-padding--lg" style="padding-top: 15px !important;">
        <div class="container">

            <div class="row galler__wrap masonry__wrap mt--80">
                @foreach($items as $item)
                <!-- Start Single Gallery -->
                <div class="col-lg-4 col-md-4 col-sm-6 col-6 gallery__item gallery--{{$item->id}}" >
                    <div class="gallery" style="padding: 0 5px">
                        <div class="gallery__thumb" style="margin-top: 30px;display: inline-block">
                            <a href="#">
                                @if(!$item->Hasmedia('images'))
                                    <img src="{{asset('img/no-img.gif')}}" alt="{{$item->title}}" title="{{$item->title}}">
                                @else
                                    <img src="{{$item->getFirstMediaUrl('images')}}" alt="{{$item->title}}" title="{{$item->title}}">
                                @endif

                            </a>
                        </div>
                        <div class="gallery__hover__inner">
                            <div class="gallery__hover__action">
                                <ul class="gallery__zoom">
                                    @if(!$item->Hasmedia('images'))
                                        <li><a href="{{asset('img/no-img.gif')}}" data-lightbox="grportimg" data-title="{{$item->title}}"><i class="fa fa-search"></i></a></li>
                                    @else
                                        <li><a href="{{$item->getFirstMediaUrl('images')}}" data-lightbox="grportimg" data-title="{{$item->title}}"><i class="fa fa-search"></i></a></li>

                                    @endif

                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- End Single Gallery -->
                    @endforeach


            </div>
        </div>
    </div>
    <!-- End Our Gallery Area -->


@endsection



