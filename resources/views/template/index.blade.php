﻿@extends('template.app')

@section('content')

    @include('template.sections.sliders')
    <div class="hidden-text">

        <h1>آموزش رباتیک</h1>
        <h2>رباتیک دانش آموزی</h2>
        <h2>مسابقات رباتیک نادر نوبخت</h2>

    </div>
            <div class="container">
                <div class="owl-carousel owl-theme">
                    @foreach($customers as $customer)
                    <div class="item" style="width: 150px;margin: 0 auto">
                        @if(!$customer->Hasmedia('images'))
                            <img  src="{{asset('img/no-img.gif')}}" alt="{{$customer->title}}" title="{{$customer->title}}">
                        @else
                            <img src="{{$customer->getFirstMediaUrl('images')}}"  alt="{{$customer->title}}" title="{{$customer->title}}">
                        @endif

                    </div>
                    @endforeach

                </div>
            </div>
                <div class="container" style="margin-top: 30px">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="section__title text-center">
                                <h2 class="title__line" style="margin-bottom: 0 !important;">به سیگما خوش آمدید </h2>
                            </div>
                        </div>
                    </div>
                </div>
    <!-- End Slider Area -->

    <div class="hidden-text">
        <h3>برگزاری مسابقات</h3>
        <h3>دوره آموزشی ربات سازی</h3>
        <h3> افتخارات شرکت سیگما</h3>
    </div>
    <section class="junior__welcome__area section-padding--md" style="justify-content: flex-end">
        <div class="container">

        <div class="row">
            @foreach(showWidget('bottom slider') as $widget)
            <div class="col-lg-3 col-xs-6 col-md-6 text-center">
              <a href="{{$widget->href}}">
                  @if(!$widget->Hasmedia('images'))
                      <img class="hover-item" src="{{asset('img/no-img.gif')}}" width="180" alt="{{$widget->title}}" title="{{$widget->title}}">
                  @else
                      <img class="hover-item" src="{{$widget->getFirstMediaUrl('images')}}" width="180"  alt="{{$widget->title}}" title="{{$widget->title}}">
                  @endif

                  <h5 class="text-hover" style="margin-top: 30px">{{$widget->title}}</h5>
              </a>
            </div>
            @endforeach
        </div>
        </div>


    </section>


     @include('template.sections.brand')



    <!-- Start Welcame Area -->
    <section style="margin-top: 20px">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section__title text-center" style="margin-bottom: 20px;margin-top: 20px">
                        <h2 class="title__line" style="margin-bottom: 0 !important;height: 2px;position: relative;text-align: center;background: {{$setting->color_theme}};padding: 0;" >  <span style="font-size: 25px;position: absolute;background: #fdfdfd;padding: 0 12px;left: 50%;transform: translate(-50%, -50%);">{{dataWidget('top footer')->excerpt}}</span></h2>
                    </div>
                </div>
            </div>

            <div class="row jn__welcome__wrapper align-items-center" style="margin-top: 0 !important;justify-content:flex-end;">
                @foreach(showWidget('top footer') as $widget)


                    <div class="col-md-6 col-lg-6 col-sm-6 md-mt-40 sm-mt-40">
                        <div class="jnr__Welcome__thumb wow fadeInRight text-center">
                            <a href="{{$widget->href}}" target="_blank">
                                @if(!$widget->Hasmedia('images'))
                                    <img class="zoom-item" src="{{asset('img/no-img.gif')}}" alt="{{$widget->title}}" title="{{$widget->title}}" width="450">
                                @else
                                    <img class="zoom-item" src="{{$widget->getFirstMediaUrl('images')}}" alt="{{$widget->title}}" title="{{$widget->title}}" width="450">
                                @endif

                            </a>
                            @if($widget->order!=4)
                                <div class="text-center" style="padding-top: 15px">
                                    <a href="{{$widget->href}}" target="_blank">
                                        <h6 class="action-item" style="margin-right: 15px;margin-left: 15px;">{{$widget->title}}</h6>
                                    </a>
                                </div>
                            @endif
                        </div>
                    </div>


                @endforeach
            </div>
        </div>

    </section>
    <!-- End Welcame Area -->
                <!-- Start Welcame Area -->
                <section style="margin-top: 20px">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="section__title text-center" style="margin-bottom: 20px;margin-top: 20px">
                                    <h2 class="title__line" style="margin-bottom: 0 !important;height: 2px;position: relative;text-align: center;background: {{$setting->color_theme}};padding: 0;" >  <span style="font-size: 25px;position: absolute;background: #fdfdfd;padding: 0 12px;left: 50%;transform: translate(-50%, -50%);">{{dataWidget('banner slider')->excerpt}}</span></h2>
                                </div>
                            </div>
                        </div>

                        <div class="row jn__welcome__wrapper align-items-center" style="margin-top: 0 !important;justify-content:flex-end;">
                        @foreach(showWidget('banner slider') as $widget)


                                   <div class="col-md-6 col-lg-6 col-sm-6 md-mt-40 sm-mt-40">
                                       <div class="jnr__Welcome__thumb wow fadeInRight text-center">
                                          <a href="{{$widget->href}}" target="_blank">
                                              @if(!$widget->Hasmedia('images'))
                                                  <img class="zoom-item" src="{{asset('img/no-img.gif')}}" alt="{{$widget->title}}" title="{{$widget->title}}" width="500">
                                              @else
                                                  <img class="zoom-item" src="{{$widget->getFirstMediaUrl('images')}}" alt="{{$widget->title}}" title="{{$widget->title}}" width="500">
                                              @endif

                                          </a>
                                          @if($widget->order!=4)
                                               <div class="text-center" style="padding-top: 15px">
                                                   <a href="{{$widget->href}}" target="_blank">
                                                    <h6 class="action-item" style="margin-right: 15px;margin-left: 15px;">{{$widget->title}}</h6>
                                                   </a>
                                               </div>
                                           @endif
                                       </div>
                                   </div>


                        @endforeach
                        </div>
                    </div>

                </section>
                <!-- End Welcame Area -->



@include('template.sections.services')

@include('template.sections.articles')


    <!-- Start Team Area -->
    <section class="dcare__team__area pb--150 bg--white" style="direction: rtl">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-sm-12 col-md-12">
                    <div class="section__title text-center" style="padding-top: 48px">
                        <h2 class="title__line" style="margin-bottom: 0 !important;height: 2px;position: relative;text-align: center;background: {{$setting->color_theme}};padding: 0;" >  <span style="font-size: 25px;position: absolute;background: #fdfdfd;padding: 0 12px;left: 50%;transform: translate(-50%, -50%);">کادر اجرایی و اموزشی</span></h2>

                    </div>
                </div>
            </div>
            <div class="row mt--40">

                <!-- Start Single Team -->
                @foreach($except_professors as $excerpt)
                    <div class="col-lg-2 col-md-6 col-sm-6 col-12">
                        <div class="team">
                            <div class="team__thumb">
                                <a href="#">
                                    @if(!$excerpt->Hasmedia('images'))
                                        <img src="{{asset('img/no-img.gif')}}" alt="{{$excerpt->full_name}}" title="{{$excerpt->full_name}}">
                                    @else
                                        <img src="{{$excerpt->getFirstMediaUrl('images')}}" alt="{{$excerpt->full_name}}" title="{{$excerpt->full_name}}">
                                    @endif

                                </a>
                                <div class="team__hover__action">

                                    <div class="team__hover__inner">
                                        <div class="text-right" style="padding-top: 10px">
                                            <h6>{{$excerpt->full_name}}</h6>
                                        </div>
                                        <div class="text-right" style="padding-top: 10px">
                                            <h6>{{$excerpt->info->about}}</h6>
                                        </div>
                                        <div class="text-right" style="padding-top: 10px">
                                            <h6>{{$excerpt->role_name}}</h6>
                                        </div>
                                        <ul class="dacre__social__link--2 d-flex justify-content-center" style="padding-top: 5px">
                                            <li class="telegram"><a href="https://telegram.me/{{$excerpt->info->telegram}}" style="position: relative"><i class="fa fa-telegram center"></i></a></li>
                                            <li class="instagram"><a href="https://www.instagram.com/{{$excerpt->info->instagram}}" style="position: relative"><i class="fa fa-instagram center"></i></a></li>
                                            <li class="whatsapp"><a href="https://wa.me/{{$excerpt->info->whatsapp}}" style="position: relative"><i class="fa fa-whatsapp center"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="team__details">
                                <div class="team__info">
                                    <h6><a href="#">{{$excerpt->full_name}}</a></h6>
                                </div>
                            </div>
                        </div>
                    </div>
            @endforeach
            <!-- End Single Team -->
            </div>
        </div>
    </section>
    <!-- End Team Area -->

    <!-- Start Team Area -->
    <section class="dcare__team__area pb--150 bg--white" style="direction: rtl">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-sm-12 col-md-12">
                    <div class="section__title text-center" style="padding-top: 25px">
                        <h2 class="title__line" style="margin-bottom: 0 !important;height: 2px;position: relative;text-align: center;background: {{$setting->color_theme}};padding: 0;" >  <span style="font-size: 25px;position: absolute;background: #fdfdfd;padding: 0 12px;left: 50%;transform: translate(-50%, -50%);">اساتید ما</span></h2>

                    </div>
                </div>
            </div>
            <div class="row mt--40">

                <!-- Start Single Team -->
                @foreach($professors as $professor)
                    <div class="col-lg-2 col-md-6 col-sm-6 col-12">
                        <div class="team">
                            <div class="team__thumb">
                                <a href="#">
                                    @if(!$professor->Hasmedia('images'))
                                        <img src="{{asset('img/no-img.gif')}}"  alt="{{$professor->full_name}}" title="{{$professor->full_name}}">
                                    @else
                                        <img src="{{$professor->getFirstMediaUrl('images')}}" alt="{{$professor->full_name}}" title="{{$professor->full_name}}">
                                    @endif

                                </a>
                                <div class="team__hover__action">

                                    <div class="team__hover__inner">
                                        <div class="text-right" style="padding-top: 10px">
                                            <h6>{{$professor->full_name}}</h6>
                                        </div>
                                        <div class="text-right" style="padding-top: 10px">
                                            <h6>{{$professor->info->about}}</h6>
                                        </div>
                                        <div class="text-right" style="padding-top: 10px">
                                            <h6>{{$professor->role_name}}</h6>
                                        </div>
                                        <ul class="dacre__social__link--2 d-flex justify-content-center" style="padding-top: 5px">
                                            <li class="telegram"><a href="https://telegram.me/{{$professor->info->telegram}}" style="position: relative"><i class="fa fa-telegram center"></i></a></li>
                                            <li class="instagram"><a href="https://www.instagram.com/{{$professor->info->instagram}}" style="position: relative"><i class="fa fa-instagram center"></i></a></li>
                                            <li class="whatsapp"><a href="https://wa.me/{{$professor->info->whatsapp}}" style="position: relative"><i class="fa fa-whatsapp center"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="team__details">
                                <div class="team__info">
                                    <h6><a href="#">{{$professor->full_name}}</a></h6>
                                </div>
                            </div>
                        </div>
                    </div>
            @endforeach
            <!-- End Single Team -->
            </div>
        </div>
    </section>
    <!-- End Team Area -->


@endsection

@section('script')

    <script src="{{asset('template/js/owl.carousel.min.js')}}"></script>

    <script>
        var owl = $('.owl-carousel');
        owl.owlCarousel({
            items:4,
            loop:true,
            margin:10,
            autoplay:true,
            autoplayTimeout:1000,
            autoplayHoverPause:true,
            navigation : false
        });
        $('.owl-carousel').owlCarousel({
            loop:true,
            margin:10,
            nav:true,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:3
                },
                1000:{
                    items:5
                }
            }
        })
    </script>

@endsection


@section('head')

    <style>
        .slider-template {
            background-repeat: no-repeat;
            background-size: cover;
            background-position: center center;
        }
        .owl-dots{
            display: none;
        }


    </style>

@endsection




