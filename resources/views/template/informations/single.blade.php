@extends('template.app')

@section('content')

    <!-- Start Blog Grid Area -->
    <div class="dcare__blog__list bg--white section-padding--lg" style="padding-top: 30px !important;">
        <div class="container">
            <div class="row">
                <!-- Start BLog Details -->
                <div class="col-lg-9 text-right" style="direction:rtl">
                    <div class="page__blog__details">
                        <article class="dacre__blog__details">
                            <div class="blog__thumb">
                                @if(!$item->Hasmedia('images'))
                                    <img src="{{asset('template/images/blog/big-img/6.jpg')}}" alt="logo images">
                                @else

                                    <img src="{{$item->getFirstMediaUrl('images')}}" alt="blog images" >
                                @endif

                            </div>
                            <div class="blog__inner text-right">
                                <h2>{{$item->title}}</h2>
                                <ul>
                                    <li>{{Morilog\Jalali\Jalalian::forge($item->created_at)->format('%A, %d %B %y')}}</li>
                                    <li><a href="#">بازدید : {{$item->view}}</a></li>
                                </ul>

    {!! $item->text !!}


                            </div>
                        </article>




                        <div class="blog__page__btn">

                            <div class="blog__share">
                                <span>اشتراک:</span>
                                <ul class="dacre__social__link--2 bg--2 d-flex justify-content-end">
                                    <li class="twitter"><a target="_blank" href='https://telegram.org/js/telegram-widget.js?14"data-telegram-share-url="{{\Request::url()}}"'><i class="fa fa-telegram"></i></a></li>
                                    <li class="vimeo"><a target="_blank" href="whatsapp://send?text={{\Request::url()}}"><i class="fa fa-whatsapp"></i></a></li>
                                </ul>
                            </div>
                            <div class="blog__share">
                                <span>{{$item->refer_link}}</span>

                            </div>
                        </div>



                        <!-- Start Latest Blog -->
                        <div class="blog__latest">
                            <h4 class="title__line--3"> آخرین اخبار</h4>
                            <div class="row">

                            @foreach($last_informations as $last_information)
                                <!-- Start Single BLog -->
                                <div class="col-12 col-lg-5 col-md-5">
                                    <article class="blog__single blog__item">
                                        <div class="blog__thumb">
                                            <a href="{{route('informations.single',['information'=>$last_information->slug])}}">
                                                @if(!$last_information->Hasmedia('images'))
                                                    <img src="{{asset('template/images/blog/big-img/10.jpg')}}" alt="{{$last_information->title}}" width="370" height="215">
                                                @else

                                                    <img src="{{$last_information->getFirstMediaUrl('images')}}" alt="{{$last_information->title}}" width="370" height="215">
                                                @endif
                                            </a>
                                        </div>
                                        <div class="blog__content">
                                            <h2><a href="{{route('informations.single',['information'=>$last_information->slug])}}">{{$last_information->title}}</a></h2>
                                            <ul class="bl__post" style="padding-right: 10px">
                                                <li>تاریخ : {{Morilog\Jalali\Jalalian::forge($last_information->created_at)->format('%A, %d %B %y')}}</li>
                                            </ul>
                                           <p>
                                                {{  \Illuminate\Support\Str::limit(strip_tags($last_information->excerpt), 30) }}

                                           </p>
                                            <div class="blog__btn">
                                                <a class="dcare__btn btn__f6f6f6" href="{{route('informations.single',['information'=>$last_information->slug])}}">ادامه مطلب</a>

                                            </div>
                                        </div>
                                    </article>
                                </div>
                                <!-- End Single BLog -->
                                    @endforeach



                            </div>
                        </div>
                        <!-- End Latest Blog -->


                    </div>
                </div>
                <!-- End BLog Details -->
                <!-- Start Sidebar -->
                <div class="col-lg-3 text-right" style="direction: rtl">
                    <div class="sidebar__widgets sidebar--right">


                        <!-- Single Widget -->
                        <div class="single__widget offer">
                            <div class="about__content">
                               <a class="ad" href="{{getAd('2b8qlPss6K2KOKzgkuT5KzuiONCBYLtEzAv')->href}}" target="_blank" data-param="2b8qlPss6K2KOKzgkuT5KzuiONCBYLtEzAv">
                                   <img src="{{getAd('2b8qlPss6K2KOKzgkuT5KzuiONCBYLtEzAv')->getFirstMediaUrl('images')}}" alt="{{$last_information->title}}">
                               </a>

                            </div>
                        </div>
                        <!-- End Widget -->


                        <!-- Single Widget -->
                        <div class="single__widget offer">
                            <div class="offer__thumb">
                                <a class="ad" href="{{getAd('GkAlPz6rLUolatJsiLxFJwW1AZhfqEP0f82')->href}}" target="_blank" data-param="GkAlPz6rLUolatJsiLxFJwW1AZhfqEP0f82">
                                    <img src="{{getAd('GkAlPz6rLUolatJsiLxFJwW1AZhfqEP0f82')->getFirstMediaUrl('images')}}" alt="{{$last_information->title}}">
                                </a>

                            </div>
                        </div>
                        <!-- End Widget -->



                        <!-- Single Widget -->
                        <div class="single__widget Popular__classes">
                            <h4>اخبار پربازدید</h4>
                            <ul>
                                @foreach($last_informations as $last_information)
                                <li><a href="{{route('informations.single',['information'=>$last_information->slug])}}">{{$last_information->title}}</a></li>

                                @endforeach
                            </ul>
                        </div>
                        <!-- End Widget -->

                        <br>
                        <br>

                        <!-- Single Widget -->
                        <div class="single__widget tags text-right">
                            <h4>برچسب ها</h4>
                            <ul style="justify-content: flex-start">
                                @foreach($item->tags as $tag)
                                    <li><a href="#">{{$tag->name}}</a></li>
                                @endforeach
                            </ul>
                        </div>
                        <!-- End Widget -->



                    </div>
                </div>
                <!-- End Sidebar -->
            </div>
        </div>
    </div>
    <!-- End Blog Grid Area -->


@endsection

@section('head')

    <meta name="csrf-token" content="{{ csrf_token() }}">

@endsection

@section('script')


    <script>
        $("a.ad").bind("click", function () {
            $code=$(this).data('param');
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type:'POST',
                contentType:'application/json; charset=utf-8',
                url:'/ajax/count/ad/'+$code,
                data: { field1:$code} ,
                success: function (response) {
                },
                error: function (response) {

                },
            });
        });


    </script>

@endsection
