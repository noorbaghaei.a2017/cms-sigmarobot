@extends('template.app')

@section('content')

    <div class="hidden-text">

        <h1>آموزش رباتیک</h1>
        <h1>رباتیک دانش آموزی</h1>
    </div>
    <!-- End Bradcaump area -->
    <!-- Start Choose Us Area -->
    <section class="dcare__choose__us__area section-padding--lg bg--white">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-sm-12 col-md-12">
                    <div class="section__title text-center" style="padding-top: 25px">
                        <h2 class="title__line">افتخارات سیگما</h2>
                    </div>
                </div>
            </div>
            <div class="row mt--40">
                <!-- Start Single Choose Option -->
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="dacre__choose__option">
                        <!-- Start Single Choose -->
                        @foreach($properties as $key=>$property)
                        <div class="choose">
                            <div class="choose__inner">
                                <h4><a href="{{route('properties.single',['property'=>$property->slug])}}">{{$property->title}}</a></h4>
                                <p>
                                    {{$property->excerpt}}
                                </p>
                            </div>
                            <div class="choose__icon">
                                <img src="{{asset('template/images/choose/icon/'.($key+1).'.png')}}" alt="choose icon">
                            </div>
                        </div>
                       @endforeach
                        <!-- End Single Choose -->

                    </div>
                </div>
                <!-- End Single Choose Option -->
                <!-- Start Single Choose Option -->
                <div class="col-lg-6 col-md-6 col-sm-12 d-block d-lg-block d-md-none">
                    <div class="dacre__choose__option">
                        <div class="choose__big__img">
                            <img src="{{asset('template/images/about-user.jpg')}}" alt="choose images">
                        </div>
                    </div>
                </div>
                <!-- End Single Choose Option -->

            </div>
        </div>
    </section>
    <!-- End Choose Us Area -->



    <!-- Start Counter Up Area -->
    <section class="dcare__counterup__area section-padding--lg bg-image--6" style="direction: rtl;border-top: 2px solid #800080;padding-top: 20px !important;">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-lg-12 col-sm-12">
                    <div class="counterup__wrapper d-flex flex-wrap flex-lg-nowrap flex-md-nowrap justify-content-between">
                        <!-- Start Single Fact -->
                        <div class="funfact">
                            <div class="fact__icon">
                                <img src="{{asset('template/images/counter/baby.png')}}" alt="flat icon" width="150">
                            </div>
                            <div class="fact__count ">
                                <span class="count">{{500+$clientscount}}</span>
                            </div>
                            <div class="fact__title">
                                <h5>دانش آموزان</h5>
                            </div>
                        </div>
                        <!-- End Single Fact -->
                        <!-- Start Single Fact -->
                        <div class="funfact">
                            <div class="fact__icon">
                                <img src="{{asset('template/images/counter/class.jpg')}}" alt="flat icon" width="150">
                            </div>
                            <div class="fact__count ">
                                <span class="count color--2">{{ $classroomscount}}</span>
                            </div>
                            <div class="fact__title">
                                <h5>کلاس ها</h5>
                            </div>
                        </div>
                        <!-- End Single Fact -->
                        <!-- Start Single Fact -->
                        <div class="funfact">
                            <div class="fact__icon">
                                <img src="{{asset('template/images/counter/teacher.jpg')}}" alt="flat icon" width="150">
                            </div>
                            <div class="fact__count ">
                                <span class="count color--3">{{$professorscount}}</span>
                            </div>
                            <div class="fact__title">
                                <h5>اساتید</h5>
                            </div>
                        </div>
                        <!-- End Single Fact -->
                        <!-- Start Single Fact -->
                        <div class="funfact">
                            <div class="fact__icon">
                                <img src="{{asset('template/images/counter/game.jpg')}}" alt="flat icon" width="150">
                            </div>
                            <div class="fact__count">
                                <span class="count color--4">{{ $racescount}}</span>
                            </div>
                            <div class="fact__title">
                                <h5>مسابقات</h5>
                            </div>
                        </div>
                        <!-- End Single Fact -->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Counter Up Area -->



@endsection
