
@extends('template.app')

@section('content')

    <div class="hidden-text">

        <h1>آموزش رباتیک</h1>
        <h1>رباتیک دانش آموزی</h1>
    </div>

    <section class="page__contact bg--white section-padding--lg">
        <div class="container" style="direction: rtl;margin-bottom: 50px;margin-top: 100px">
            <div class="row">
                <!-- Start Single Address -->
                <div class="col-md-6 col-sm-6 col-12 col-lg-4">
                    <div class="address location">
                        <div class="ct__icon">
                            <i class="fa fa-home"></i>
                        </div>
                        <div class="address__inner">
                            <h2>موقعیت ما</h2>
                            <p>آدرس شرکت سیگما رباتیک</p>
                            <ul>
                                <li>{{$setting->address}}</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- End Single Address -->
                <!-- Start Single Address -->
                <div class="col-md-6 col-sm-6 col-12 col-lg-4 xs-mt-60">
                    <div class="address phone">
                        <div class="ct__icon">
                            <i class="fa fa-phone"></i>
                        </div>
                        <div class="address__inner">
                            <h2>شماره تماس</h2>
                            <p>راه های ارتباط تلفنی</p>
                            <ul>
                                @if(isset(json_decode($setting->mobile,true)[0]))
                                <li><a href="tell:{{json_decode($setting->mobile,true)[0]}}">{{json_decode($setting->mobile,true)[0]}}</a></li>
                                @endif
                                @if(isset(json_decode($setting->phone,true)[0]))
                                <li><a href="tell:+98{{json_decode($setting->phone,true)[0]}}"> {{json_decode($setting->phone,true)[0]}}</a></li>
                                    @endif
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- End Single Address -->
                <!-- Start Single Address -->
                <div class="col-md-6 col-sm-6 col-12 col-lg-4 md-mt-60 sm-mt-60">
                    <div class="address email">
                        <div class="ct__icon">
                            <i class="fa fa-envelope"></i>
                        </div>
                        <div class="address__inner">
                            <h2>آدرس ایمیل</h2>
                            <p>راه های ارتباطی ما در آدرس الکترونیکی</p>
                            <ul>
                                <li><a href="mailto::{{$setting->email}}">{{$setting->email}}</a></li>

                            </ul>
                        </div>
                    </div>
                </div>
                <!-- End Single Address -->
            </div>
        </div>
    </section>
    <div class="contact__map">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6">

                            {!! $setting->google_map !!}

                </div>
                <div class="col-lg-6">
                    <div class="child__image">
                        <img src="{{asset('template/images/about-us.jpg')}}" alt="children images">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Start Contact Form -->
    <section class="contact__box section-padding--lg bg-image--27">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-sm-12 col-md-12">
                    <div class="section__title text-center">
                        <h2 class="title__line">منتظر انتقاد و پیشنهادات شما هستیم</h2>
                    </div>
                </div>
            </div>
            <div class="row mt--80">
                <div class="col-lg-12">
                    <div class="contact-form-wrap">
                        <form action="{{route('send.idea')}}" method="get" style="direction: rtl">
                            <div class="single-contact-form name">
                                <input type="text" name="name" placeholder="نام و نام خانوادگی">
                                <input type="email" name="email" placeholder="ایمیل">
                            </div>
                            <div class="single-contact-form subject">
                                <input type="text" name="subject" placeholder="موضوع">
                            </div>
                            <div class="single-contact-form message">
                                <textarea name="message"  placeholder="متن مورد نظر را وارد کنید"></textarea>
                            </div>
                            <div class="contact-btn">
                                <button type="submit" class="btn btn-success btn-block" >ارسال</button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Contact Form -->


@endsection

@section('script')

    <!-- Google Map js -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBmGmeot5jcjdaJTvfCmQPfzeoG_pABeWo"></script>
    <script>
        // When the window has finished loading create our google map below
        google.maps.event.addDomListener(window, 'load', init);

        function init() {
            // Basic options for a simple Google Map
            // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
            var mapOptions = {
                // How zoomed in you want the map to start at (always required)
                zoom: 12,

                scrollwheel: false,

                // The latitude and longitude to center the map (always required)
                center: new google.maps.LatLng(23.7286, 90.3854), // New York

                // How you would like to style the map.
                // This is where you would paste any style found on Snazzy Maps.
                styles:
                    [
                        {
                            "featureType": "all",
                            "elementType": "all",
                            "stylers": [
                                {
                                    "invert_lightness": true
                                },
                                {
                                    "saturation": 10
                                },
                                {
                                    "lightness": 30
                                },
                                {
                                    "gamma": 0.5
                                },
                                {
                                    "hue": "#435158"
                                }
                            ]
                        },
                        {
                            "featureType": "administrative.province",
                            "elementType": "labels.text.stroke",
                            "stylers": [
                                {
                                    "hue": "#ff00d0"
                                }
                            ]
                        }
                    ]
            };

            // Get the HTML DOM element that will contain your map
            // We are using a div with id="map" seen below in the <body>
            var mapElement = document.getElementById('googleMap');

            // Create the Google Map using our element and options defined above
            var map = new google.maps.Map(mapElement, mapOptions);

            // Let's also add a marker while we're at it
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(23.7286, 90.3854),
                map: map,
                title: 'Dcare!',
                icon: 'images/icons/map.png',
                animation:google.maps.Animation.BOUNCE

            });
        }
    </script>


@endsection
