﻿@extends('template.app')
@section('content')
    <section class="dcare__blog__area section-padding--lg bg--white">
        <div class="container">
            <div class="row blog-page text-right" style="direction: rtl">
    {!! $page->text !!}
            </div>
        </div>
    </section>

@endsection


@section('seo')


    <meta name="author" content="amin nourbaghaei">

    <meta name="robots" content="noindex,nofollow" />

@endsection
