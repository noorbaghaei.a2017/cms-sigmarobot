@extends('template.app')

@section('content')


    <!-- Start Blog Area -->
    <section class="dcare__blog__area section-padding--lg bg--white" style="padding-top: 15px !important;">
        <div class="container">
            <div class="row blog-page text-right" style="direction: rtl">
                <!-- Start Single Blog -->
                @foreach($items as $item)
                <div class="col-lg-4 col-md-6 col-sm-12">
                    <div class="blog__2">
                        <div class="blog__thumb">
                            <a href="{{route('articles.single',['article'=>$item->slug])}}">

                                @if(!$item->Hasmedia('images'))
                                    <img src="{{asset('template/images/blog/bl-2/1.jpg')}}" alt="blog images">
                                @else

                                    <img src="{{$item->getFirstMediaUrl('images')}}" alt="blog images" >
                                @endif

                            </a>
                        </div>
                        <div class="blog__inner">
                            <div class="blog__hover__inner">
                                <h2><a href="{{route('articles.single',['article'=>$item->slug])}}">{{$item->title}}</a></h2>
                                <div class="bl__meta">
                                    <p> نویسنده : {{$item->author}} </p>
                                    <p> تاریخ : {{Morilog\Jalali\Jalalian::forge($item->start_at)->format('%A, %d %B %y')}}</p>
                                </div>
                                <div class="bl__details">
                                    <p>{{$item->excerpt}}</p>
                                </div>
                                <div class="blog__btn">
                                    <a class="bl__btn" href="{{route('articles.single',['article'=>$item->slug])}}">ادامه مطلب</a>
                                    <a class="bl__share__btn" href="#"><i class="fa fa-share-alt"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Single Blog -->
                @endforeach

            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="dcare__pagination mt--80">
                        <ul class="dcare__page__list d-flex justify-content-center">
                            <li><a href="#"><span class="ti-angle-double-left"></span></a></li>
                            <li><a class="page" href="#">Prev</a></li>
                            <li><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#"><i class="fa fa-ellipsis-h"></i></a></li>
                            <li><a href="#">28</a></li>
                            <li><a class="page" href="#">Next</a></li>
                            <li><a href="#"><span class="ti-angle-double-right"></span></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Blog Area -->
    <!-- Start Subscribe Area -->
    <section class="bcare__subscribe bg-image--7 subscrive--2">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-sm-12 col-lg-12">
                    <div class="subscribe__inner">
                        <h2>Subscribe To Our Newsletter</h2>
                        <div class="newsletter__form">
                            <div class="input__box">
                                <div id="mc_embed_signup">
                                    <form action="http://devitems.us11.list-manage.com/subscribe/post?u=6bbb9b6f5827bd842d9640c82&amp;id=05d85f18ef" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                                        <div id="mc_embed_signup_scroll" class="htc__news__inner">
                                            <div class="news__input">
                                                <input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="Enter Your E-mail" required>
                                            </div>
                                            <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                                            <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_6bbb9b6f5827bd842d9640c82_05d85f18ef" tabindex="-1" value=""></div>
                                            <div class="clearfix subscribe__btn"><input type="submit" value="Send Now" name="subscribe" id="mc-embedded-subscribe" class="bst__btn btn--white__color">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Subscribe Area -->




@endsection



