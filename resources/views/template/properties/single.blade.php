@extends('template.app')

@section('content')

    <!-- Start Blog Grid Area -->
    <div class="dcare__blog__list bg--white section-padding--lg" style="padding-top: 15px !important;">
        <div class="container">
            <div class="row">
                <!-- Start BLog Details -->
                <div class="col-lg-9 text-right" style="direction:rtl">
                    <div class="page__blog__details">
                        <article class="dacre__blog__details">
                            <div class="blog__thumb">
                                @if(!$item->Hasmedia('images'))
                                    <img src="{{asset('template/images/blog/big-img/6.jpg')}}" alt="logo images">
                                @else

                                    <img src="{{$item->getFirstMediaUrl('images')}}" alt="blog images" >
                                @endif

                            </div>
                            <div class="blog__inner">
                                <h2>{{$item->title}}</h2>

    {!! $item->text !!}


                            </div>
                        </article>



                    </div>
                </div>
                <!-- End BLog Details -->
                <!-- Start Sidebar -->
                <div class="col-lg-3 text-right" style="direction: rtl">
                    <div class="sidebar__widgets sidebar--right">


                        <!-- Single Widget -->
                        <div class="single__widget about">
                            <div class="about__content">
                                <img src="{{asset('template/images/others/1.jpg')}}" alt="">

                            </div>
                        </div>
                        <!-- End Widget -->


                        <!-- Single Widget -->
                        <div class="single__widget offer">
                            <div class="offer__thumb">
                                <img src="{{asset('template/images/others/2.jpg')}}" alt="">
                            </div>
                        </div>
                        <!-- End Widget -->



                    </div>
                </div>
                <!-- End Sidebar -->
            </div>
        </div>
    </div>
    <!-- End Blog Grid Area -->


@endsection
