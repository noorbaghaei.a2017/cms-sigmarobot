@extends('template.app')

@section('content')
    <div class="hidden-text">

        <h1>آموزش رباتیک</h1>
        <h1>رباتیک دانش آموزی</h1>
    </div>
    <!-- Start Class Details -->
    <section class="page-class-details bg--white section-padding--lg" style="padding-top: 15px !important;">
        <div class="container">
            <div class="row">
                <div class="col-lg-9 text-right" style="direction: rtl">
                    <div>
                        @include('core::layout.alert-success')
                        @include('core::layout.alert-danger')
                    </div>
                    <div class="class__quality__desc">

                        <div class="class__thumb">
                            <div class="courses__type d-flex justify-content-between">
                                <ul class="d-flex">
                                    <li>دسته بندی : <span>{{$item->nameCategory}}</span></li>
                                    <span>/</span>
                                    <li>تاریخ شروع : <span>{{Morilog\Jalali\Jalalian::forge($item->start_at)->format('%A, %d %B %y')}}</span></li>
                                </ul>
                                <ul class="rating d-flex">
                                    <li><span class="ti-star"></span></li>
                                    <li><span class="ti-star"></span></li>
                                    <li><span class="ti-star"></span></li>
                                    <li><span class="ti-star"></span></li>
                                    <li><span class="ti-star"></span></li>
                                </ul>
                            </div>
                            <div class="courses__images">
                                @if(!$item->Hasmedia('images'))
                                    <img src="{{asset('img/no-img.gif')}}" alt="class images">
                                @else

                                    <img src="{{$item->getFirstMediaUrl('images')}}" alt="class images" >
                                @endif

                            </div>

                        </div>
                        <div class="class-details-inner">
                            <div class="courses__inner">
                                <h2>{{$item->title}}</h2>
                                <ul>
                                    <li>مدت زمان  : <span>{{$item->total_hour}}</span>  ساعت </li>
                                    <li>تاریخ پایان  :  <i class="fa fa-calendar"></i><span>{{Morilog\Jalali\Jalalian::forge($item->end_at)->format('%A, %d %B %y')}}</span></li>
                                </ul>
                                <p>
                                    محل برگزاری :  {{$item->event_place}}
                                </p>

                                    {!! $item->text !!}

                            </div>

                            <div class="class__nav nav justify-content-start" role="tablist">
                                <a class="nav-item nav-link active" data-toggle="tab" href="#nav-view" role="tab">توضیحات</a>
                                <a class="nav-item nav-link " data-toggle="tab" href="#nav-student" role="tab">دانش آموزان</a>

                                @if(hasChild($item,'race'))
                                    <a class="nav-item nav-link " data-toggle="tab" href="#nav-child" role="tab">زیر دوره ها</a>
                                @endif

                            </div>

                            <div class="class__tab__content--inner">
                                <div class="class__view single-class-content tab-pane fade show active" id="nav-view" role="tablist">
                                    <h2>توضیحات</h2>

                                    <p>
                                        {!! $item->excerpt !!}
                                    </p>

                                    <div class="share__us">
                                        <span>اشتراک گذاری</span>

                                        <ul class="dacre__social__link d-flex justify-content-center">
                                            <li class="whatsapp"><a target="_blank" href="whatsapp://send?text={{\Request::url()}}"><i class="fa fa-whatsapp"></i></a></li>
                                            <li class="whatsapp"><a target="_blank" href='https://telegram.org/js/telegram-widget.js?14"data-telegram-share-url="{{\Request::url()}}"'><i class="fa fa-telegram"></i></a></li>
                                        </ul>
                                    </div>

                                </div>

                                <div class="teacher__view single-class-content tab-pane fade" id="nav-student" role="tablist">

                                    <!-- Start Dingle Teacher -->
                                    <div class="student">
                                        <div class="container">

                                            <div class="row">
                                                @foreach($item->students as $value)
                                                    <div class="col-lg-3 col-xs-6 col-md-6 text-center">

                                                        @if(!$value->student->Hasmedia('images'))
                                                            <img class="hover-item" src="{{asset('img/no-img.gif')}}" width="180" alt="{{$value->student->full_name}}" title="{{$value->student->full_name}}">
                                                        @else
                                                            <img class="hover-item" src="{{$value->student->getFirstMediaUrl('images')}}" width="180"  alt="{{$value->student->full_name}}" title="{{$value->student->full_name}}">
                                                        @endif

                                                        <h5 class="text-hover" style="margin-top: 30px">{{$value->student->full_name}}</h5>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>

                                    </div>
                                    <!-- End Dingle Teacher -->




                                </div>

                                @if(hasChild($item,'race'))

                                    <div class="teacher__view single-class-content tab-pane fade" id="nav-child" role="tablist">

                                    @foreach(getChild($item,'race') as $child)


                                                <div class="post__content" style="margin-bottom: 10px">

                                                        @if(!$child->Hasmedia('images'))
                                                            <img src="{{asset('img/no-img.gif')}}" alt="{{$child->title}}" title="{{$child->title}}" width="50" >
                                                        @else

                                                            <img src="{{$child->getFirstMediaUrl('images')}}" alt="{{$child->title}}" title="{{$child->title}}" width="50" >
                                                        @endif

                                                    <span>{{$child->title}}</span>

                                                </div>




                                        @endforeach



                                    </div>

                                @endif
                            </div>
                        </div>
                        <!-- Start Related Class -->
                        <div class="related__class__wrapper">
                            <h2 class="title">مسابقه های مشابه</h2>
                            <div class="row">

                            @foreach($last_races as $last_race)
                                <!-- Start Single Classs -->
                                    <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                                        <div class="related__classs">
                                            <div class="calss__thumb">
                                                <a href="{{route('races.single',['race'=>$last_race->slug])}}">
                                                    @if(!$last_race->Hasmedia('images'))
                                                        <img src="{{asset('img/no-img.gif')}}" alt="class images" width="370" height="170">
                                                    @else

                                                        <img src="{{$last_race->getFirstMediaUrl('images','medium')}}" alt="class images" width="370" height="170">
                                                    @endif

                                                </a>
                                            </div>
                                            <div class="class__details">
                                                <h2><a href="{{route('races.single',['race'=>$last_race->slug])}}">{{$last_race->title}}</a></h2>
                                            </div>
                                            <div class="class__hover__action">
                                                <div class="classs__hover__inner">

                                                    <h2><a href="{{route('races.single',['race'=>$last_race->slug])}}">{{$last_race->title}}</a></h2>
                                                    <ul>
                                                        <li>تاریخ شروع : {{Morilog\Jalali\Jalalian::forge($item->end_at)->format('%A, %d %B %y')}}</li>
                                                        <li>ظرفیت : {{$last_race->capacity}}</li>
                                                    </ul>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End Single Classs -->


                                @endforeach

                            </div>
                        </div>
                        <!-- End Related Class -->

                    </div>
                </div>
                <!-- Start Sidebar -->
                <div class="col-lg-3">
                    <div class="sidebar__widgets sidebar--right">


                        <div>
                            @if($item->status==1)
                            @if(auth('client')->check())

                                @if(hasAction($item,auth('client')->user(),'race'))

                                    <span class="btn btn-block btn-primary">شما در این مسابقه ثبت نام کردید</span>

                                @else

                                    @if($item->price->amount > 0)
                                        <div class="text-center">
                                            <p>  <span style="color: red;font-size: 26px">  {{number_format($item->price->amount)}} </span><br><span style="font-size: 20px;">{{\Modules\Core\Entities\Currency::find($item->currency)->symbol}}</span></p>
                                        </div>
                                        <form action="{{route('send.request.transaction',['item'=>$item->token,'name'=>'race'])}}" method="POST">
                                            @csrf


                                            <button type="submit" class="btn btn-block btn-success">پرداخت شهریه و ثبت نام</button>

                                        </form>

                                    @elseif($item->price->amount==0)
                                        @if($item->status==1)
                                        <a href="{{route('register.registerRace',['race'=>$item->token])}}" class="btn btn-block btn-success">ثبت نام</a>
                                        @endif
                                    @endif



                                @endif
                                @endif


                            @else

                                <a href="{{route('client.dashboard')}}" class="btn btn-block btn-warning">وارد سایت شوید</a>

                            @endif

                        </div>
                        <!-- Single Widget -->
                        <div class="single__widget offer">
                            <div class="about__content">
                                <a class="ad" href="{{getAd('oejMSD0Mp6HjmZla3cuVMQcK0Quw857NVsR')->href}}" target="_blank" data-param="oejMSD0Mp6HjmZla3cuVMQcK0Quw857NVsR">
                                    <img src="{{getAd('oejMSD0Mp6HjmZla3cuVMQcK0Quw857NVsR')->getFirstMediaUrl('images')}}" alt="">
                                </a>
                            </div>
                        </div>
                        <!-- End Widget -->

                        <!-- Single Widget -->
                        <div class="single__widget recent__post text-right">
                            <h4>آخرین مسابقه ها</h4>
                            <ul style="direction: rtl">
                                @foreach($last_races as $last_race)

                                    <li>
                                        <a href="{{route('races.single',['race'=>$last_race->slug])}}">
                                            @if(!$last_race->Hasmedia('images'))
                                                <img src="{{asset('img/no-img.gif')}}" alt="{{$last_race->title}}" title="{{$last_race->title}}" width="95" height="65">
                                            @else

                                                <img src="{{$last_race->getFirstMediaUrl('images','thumb')}}" alt="{{$last_race->title}}" title="{{$last_race->title}}" width="95" >
                                            @endif
                                        </a>
                                        <div class="post__content">
                                            <h6><a href="{{route('races.single',['race'=>$last_race->slug])}}">{{$last_race->title}}</a></h6>
                                            <span class="date"><i class="fa fa-calendar" style="padding-right: 0 !important;padding-left: 7px"></i>{{Morilog\Jalali\Jalalian::forge($item->start_at)->format('%A, %d %B %y')}}</span>
                                        </div>

                                    </li>
                                @endforeach
                            </ul>
                        </div>
                        <!-- End Widget -->

                        <!-- Single Widget -->
                        <div class="single__widget offer">
                            <div class="offer__thumb text-right">
                                <a class="ad" href="{{getAd('wleczr5TsBHlFnsWBJFeqeEPHwpJIZ6YTxJ')->href}}" target="_blank" data-param="wleczr5TsBHlFnsWBJFeqeEPHwpJIZ6YTxJ">
                                    <img src="{{getAd('wleczr5TsBHlFnsWBJFeqeEPHwpJIZ6YTxJ')->getFirstMediaUrl('images')}}" alt="">
                                </a>
                            </div>
                        </div>
                        <!-- End Widget -->



                        <!-- Single Widget -->
                        <div class="single__widget Popular__classes text-right">
                            <h4>مسابقه های محبوب</h4>
                            <ul>
                                @foreach($last_races as $last_race)
                                <li><a href="{{route('races.single',['race'=>$last_race->slug])}}">{{$last_race->title}}</a></li>
                                @endforeach
                            </ul>
                        </div>
                        <!-- End Widget -->

                        <!-- Single Widget -->
                        <div class="single__widget tags text-right">
                            <h4>برچسب ها</h4>
                            <ul style="justify-content: flex-end">
                                @foreach($item->tags as $tag)
                                    <li><a href="#">{{$tag->name}}</a></li>
                                @endforeach
                            </ul>
                        </div>
                        <!-- End Widget -->


                    </div>
                </div>
                <!-- End Sidebar -->
            </div>
        </div>
    </section>
    <!-- End Class Details -->


@endsection
@section('head')

    <meta name="csrf-token" content="{{ csrf_token() }}">

@endsection

@section('script')


    <script>
        $("a.ad").bind("click", function () {
            $code=$(this).data('param');
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type:'POST',
                contentType:'application/json; charset=utf-8',
                url:'/ajax/count/ad/'+$code,
                data: { field1:$code} ,
                success: function (response) {
                },
                error: function (response) {

                },
            });
        });


    </script>

@endsection




