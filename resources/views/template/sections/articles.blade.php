@if(\Modules\Core\Helper\CoreHelper::hasArticle())
<!-- Start Blog Area -->
<section class="jnr__blog_area section-padding--lg bg-image--3">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-sm-12 col-md-12">
                <div class="section__title text-center white--title">
                    <h2 class="title__line" style="margin-top: 20px">آخرین مقالات</h2>
                </div>
            </div>
        </div>
        <div class="row blog__wrapper mt--40" style="direction: rtl">
            @foreach($new_articles as $article)
            <!-- Start Single Blog -->
            <div class="col-lg-4 col-md-6 col-sm-12 wow fadeInLeft">
                <article class="blog">
                    <div class="blog__date">
                        <span>تاریخ : {{Morilog\Jalali\Jalalian::forge($article->created_at)->format('%A, %d %B %y')}}</span>
                    </div>
                    <div class="blog__thumb">
                        <a href="{{route('articles.single',['article'=>$article->slug])}}">
                            @if(!$article->Hasmedia('images'))
                                <img src="{{asset('img/no-img.gif')}}" alt="logo images" width="370" height="215">
                            @else

                                <img src="{{$article->getFirstMediaUrl('images')}}" alt="logo images" width="370" height="215">
                            @endif

                        </a>
                    </div>
                    <div class="blog__inner">
                        <span>منتشر شده توسط : {{$article->user_info->full_name}}</span>
                        <h4><a href="{{route('articles.single',['article'=>$article->slug])}}">{{$article->title}}</a></h4>
                        <p>
                            {{$article->excerpt}}
                        </p>
                        <ul class="blog__meta d-flex flex-wrap flex-md-nowrap flex-lg-nowrap justify-content-between">
                            <li><a href="#">بازدید : {{$article->view}}</a></li>
                        </ul>
                    </div>
                </article>
            </div>
            <!-- End Single Blog -->
                @endforeach

        </div>
    </div>
</section>
<!-- End Blog Area -->
@endif
