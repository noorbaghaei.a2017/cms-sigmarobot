@if(\Modules\Core\Helper\CoreHelper::hasBrand())
<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section__title text-center">
                    <h2 class="title__line">برند ها</h2>
                </div>
            </div>
        </div>
        <!-- Set up your HTML -->
        <div class="owl-carousel">
            @foreach($brands as $brand)

                <div>
                    @if(!$brand->Hasmedia('images'))
                        <img href="{{$brand->href}}" src="{{asset('img/no-img.gif')}}">
                    @else
                        <img href="{{$brand->href}}" src="{{$brand->getFirstMediaUrl('images')}}" >
                    @endif

                </div>

            @endforeach
        </div>

    </div>
</section>
@endif
