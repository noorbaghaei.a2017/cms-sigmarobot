
<!-- Footer Area -->
<footer id="footer" class="footer-area footer--2">
    <div class="footer__wrapper bg-image--10 section-padding--lg" style="padding-top: 20px !important;">
        <div class="container">
            <div class="row" style="direction: rtl">


                <!-- Start Single Widget -->
                <div class="col-lg-4 col-md-6 col-sm-12 sm-mt-40 text-right">
                    <div class="footer__widget">
                        <h4 style="position: relative">آخرین اخبار<span class="bottom-line"></span></h4>
                        <div class="footer__innner">
                            <div class="ftr__latest__post">

                            @foreach($new_informations as $information)
                                <!-- Start Single -->
                                    <div class="single__ftr__post d-flex">
                                        <div class="ftr__post__thumb">
                                            <a href="{{route('informations.single',['information'=>$information->slug])}}">
                                                @if(!$information->Hasmedia('images'))
                                                    <img src="{{asset('img/no-img.gif')}}" alt="{{$information->title}}" >
                                                @else

                                                    <img src="{{$information->getFirstMediaUrl('images')}}" alt="{{$information->title}}">
                                                @endif
                                            </a>
                                        </div>
                                        <div class="ftr__post__details">
                                            <h6><a href="{{route('informations.single',['information'=>$information->slug])}}">{{$information->title}}</a></h6>
                                            <span><i class="fa fa-calendar"></i>{{Morilog\Jalali\Jalalian::forge($information->created_at)->format('%A, %d %B %y')}}</span>
                                        </div>


                                    </div>
                                    <!-- End Single -->
                                @endforeach

                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Single Widget -->
                <!-- Start Single Widget -->
                <div class="col-lg-3 col-md-6 col-sm-12 sm-mt-40 text-right">
                    <div class="footer__widget">
                        <h4 style="position: relative">آشنایی با سیگما <span class="bottom-line"></span></h4>
                        <div class="footer__innner">
                            <div class="ftr__latest__post">

                            @foreach(showWidget('bottom slider') as $widget)
                                <!-- Start Single -->
                                    <div class="single__ftr__post d-flex">
                                        <div class="ftr__post__details">
                                            <h6><a href="{{$widget->href}}" style="padding-left: 0">{{$widget->title}}</a></h6>
                                        </div>


                                    </div>
                                    <!-- End Single -->
                                @endforeach

                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Single Widget -->
                <!-- Start Single Widget -->
                <div class="col-lg-4 col-md-6 col-sm-12 text-right">
                    <div class="footer__widget">
                        <div class="ft__logo">

                        </div>
                        <div class="ftr__details">

                            <p style="font-size: 16px">
                                سیگما را در شبکه های اجتماعی دنبال کنید
                            </p>
                            <br>

                        </div>
                        <div class="ftr__address__inner">
                            <div class="footer__social__icon">
                                <ul class="dacre__social__link--2 d-flex justify-content-start">
                                    <li class="telegram"><a href="https://telegram.me/{{$setting->info->telegram}}"><i class="fa fa-telegram"></i></a></li>
                                    <li class="instagram"><a href="https://www.instagram.com/{{$setting->info->instagram}}"><i class="fa fa-instagram"></i></a></li>
                                    <li class="whatsapp"><a href="https://wa.me/{{$setting->info->whatsapp}}"><i class="fa fa-whatsapp"></i></a></li>
                                </ul>
                            </div>
                            <div>
                                <a referrerpolicy="origin" target="_blank" href="https://trustseal.enamad.ir/?id=203213&amp;Code=tgWYLmJ1pMjxPzvtDzIK"><img referrerpolicy="origin" src="https://Trustseal.eNamad.ir/logo.aspx?id=203213&amp;Code=tgWYLmJ1pMjxPzvtDzIK" alt="" style="cursor:pointer" id="tgWYLmJ1pMjxPzvtDzIK"></a>

                            </div>
                            <div class="ft__btm__title">
                              @foreach($bottom_menus as $menu)
                                    <h4><a href="{{$menu->href}}">{{$menu->symbol}}</a></h4>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Single Widget -->

            </div>
        </div>
    </div>
    <!-- .Start Footer Contact Area -->
    <div class="footer__contact__area bg__cat--2">
        <div class="container">
            <div class="row" style="direction: rtl">
                <div class="col-lg-12">
                    <div class="footer__contact__wrapper d-flex flex-wrap justify-content-between">
                        <div class="single__footer__address">
                            <div class="ft__contact__icon">
                                <i class="fa fa-home"></i>
                            </div>
                            <div class="ft__contact__details">
                                <p class="text-right" style="font-size: 16px">{{$setting->address}}</p>
                            </div>
                        </div>
                        <div class="single__footer__address">
                            <div class="ft__contact__icon">
                                <i class="fa fa-phone"></i>
                            </div>
                            <div class="ft__contact__details">
                                <p style="font-size: 16px">
                                    <a href="tel::{{isset(json_decode($setting->phone,true)[0]) ? json_decode($setting->phone,true)[0] : ""}}">{{isset(json_decode($setting->phone,true)[0]) ? json_decode($setting->phone,true)[0] : ""}}</a>
                                    <span>-</span>
                                    <a href="tel::{{isset(json_decode($setting->phone,true)[1]) ? json_decode($setting->phone,true)[1] : ""}}">{{isset(json_decode($setting->phone,true)[1]) ? json_decode($setting->phone,true)[1] : ""}}</a>

                                </p>

                            </div>
                        </div>
                        <div class="single__footer__address">
                            <div class="ft__contact__icon">
                                <i class="fa fa-envelope"></i>
                            </div>
                            <div class="ft__contact__details">
                                <p style="font-size: 16px"><a href="mailto::{{$setting->email}}">{{$setting->email}}</a></p>

                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- .End Footer Contact Area -->
    <div class="copyright  bg--theme">
        <div class="container">
            <div class="row align-items-center copyright__wrapper justify-content-center">
                <div class="col-lg-12 col-sm-12 col-md-12">
                    <div class="coppy__right__inner text-center">
                        <p>Copyright <i class="fa fa-copyright"></i> 2018 <a href="#" target="_blank" >amin nourbaghaei</a> All rights reserved. </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- //Footer Area -->
</div><!-- //Main wrapper -->

<!-- JS Files -->
<script src="{{asset('template/js/vendor/jquery-3.2.1.min.js')}}"></script>
<script src="{{asset('template/js/popper.min.js')}}"></script>
<script src="{{asset('template/js/bootstrap.min.js')}}"></script>
<script src="{{asset('template/js/plugins.js')}}"></script>
<script src="{{asset('template/js/active.js')}}"></script>



@yield('script')

</body>
</html>
