<!-- Start Our Gallery Area -->
<section class="junior__gallery__area bg--white section-padding--lg">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-sm-12 col-md-12">
                <div class="section__title text-center">
                    <h2 class="title__line">تصاویر ما</h2>
                </div>
            </div>
        </div>
        <div class="row galler__wrap mt--40">
            <!-- Start Single Gallery -->
            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                <div class="gallery wow fadeInUp">
                    <div class="gallery__thumb">
                        <a href="#">
                            <img src="{{asset('template/images/gallery/gallery-1/1.jpg')}}" alt="gallery images">
                        </a>
                    </div>
                    <div class="gallery__hover__inner">
                        <div class="gallery__hover__action">
                            <ul class="gallery__zoom">
                                <li><a href="images/gallery/big-img/1.jpg" data-lightbox="grportimg" data-title="My caption"><i class="fa fa-search"></i></a></li>
                                <li><a href="gallery-details.html"><i class="fa fa-link"></i></a></li>
                            </ul>

                        </div>
                    </div>
                </div>
            </div>
            <!-- End Single Gallery -->
            <!-- Start Single Gallery -->
            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                <div class="gallery wow fadeInUp">
                    <div class="gallery__thumb">
                        <a href="#">
                            <img src="{{asset('template/images/gallery/gallery-1/2.jpg')}}" alt="gallery images">
                        </a>
                    </div>
                    <div class="gallery__hover__inner">
                        <div class="gallery__hover__action">
                            <ul class="gallery__zoom">
                                <li><a href="images/gallery/big-img/2.jpg" data-lightbox="grportimg" data-title="My caption"><i class="fa fa-search"></i></a></li>
                                <li><a href="gallery-details.html"><i class="fa fa-link"></i></a></li>
                            </ul>

                        </div>
                    </div>
                </div>
            </div>
            <!-- End Single Gallery -->
            <!-- Start Single Gallery -->
            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                <div class="gallery wow fadeInUp">
                    <div class="gallery__thumb">
                        <a href="#">
                            <img src="{{asset('template/images/gallery/gallery-1/3.jpg')}}" alt="gallery images">
                        </a>
                    </div>
                    <div class="gallery__hover__inner">
                        <div class="gallery__hover__action">
                            <ul class="gallery__zoom">
                                <li><a href="images/gallery/big-img/3.jpg" data-lightbox="grportimg" data-title="My caption"><i class="fa fa-search"></i></a></li>
                                <li><a href="gallery-details.html"><i class="fa fa-link"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Single Gallery -->

        </div>
    </div>
</section>
<!-- End Our Gallery Area -->
