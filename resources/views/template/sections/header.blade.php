<!doctype html>
<html class="no-js" lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    {!! SEO::generate() !!}

    @yield('seo')

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicons -->

    @if(!$setting->Hasmedia('logo'))
        <link rel="shortcut icon" href="{{asset('img/no-img.gif')}}">
        <link rel="apple-touch-icon" href="{{asset('img/no-img.gif')}}">
       @else
        <link rel="shortcut icon" href="{{$setting->getFirstMediaUrl('logo')}}">
        <link rel="apple-touch-icon" href="{{$setting->getFirstMediaUrl('logo')}}">
    @endif

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Google font (font-family: 'Lobster', Open+Sans;) -->
    <link href="https://fonts.googleapis.com/css?family=Lobster+Two:400,400i,700,700i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Bubblegum+Sans" rel="stylesheet">

    <!-- Stylesheets -->
    <meta name="google-site-verification" content="{{env('GOOGLE_VERIFY')}}" />
    <link rel="stylesheet" href="{{asset('template/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('template/css/plugins.css')}}">
    <link rel="stylesheet" href="{{asset('template/css/style.css')}}">

    <!-- Cusom css -->
    <link rel="stylesheet" href="{{asset('template/css/custom.css')}}">

    <!-- Modernizer js -->
    <script src="{{asset('template/js/vendor/modernizr-3.5.0.min.js')}}"></script>

    @yield('head')

    <style>
        .container-chat {
            border: 2px solid #dedede;
            background-color: #f1f1f1;
            border-radius: 5px;
            padding: 10px;
            margin: 10px 0;
        }

        .darker {
            border-color: #ccc;
            background-color: #ddd;
        }

        .container-chat::after {
            content: "";
            clear: both;
            display: table;
        }

        .container-chat img {
            float: left;
            max-width: 60px;
            width: 100%;
            margin-right: 20px;
            border-radius: 50%;
        }

        .container-chat img.right {
            float: right;
            margin-left: 20px;
            margin-right:0;
        }

        .time-right {
            float: right;
            color: #aaa;
        }

        .time-left {
            float: left;
            color: #999;
        }
    </style>

    <style>
        .bg__cat--1{
            background: {{$setting->color_theme}};
        }
        .junior__header__top .jun__header__top__left .top__address li i {
            color: {{$setting->color_theme}};
        }
        .footer__contact__wrapper .single__footer__address .ft__contact__icon i {
            color: {{$setting->color_theme}};

        }
        .jn__welcome__wrapper .jnr__Welcome__thumb a.play__btn {
            color: {{$setting->color_theme}};
        }
        .header--one .sticky__header.is-sticky {
            background: {{$setting->color_theme}} none repeat scroll 0 0;
        }
        .dcare__btn {
            background: {{$setting->color_theme}} none repeat scroll 0 0;
        }
        a:active, a:hover {
            color: {{$setting->color_theme}};
        }
        .page-class-details .courses__inner ul li span {
            color: {{$setting->color_theme}};
        }
        .page-class-details .class__nav a.active, .page-class-details .class__nav a:hover {
            color: {{$setting->color_theme}};
        }
        .more-click{
            background: {{$setting->color_theme}};
            color: #fdfdfd;
        }
        .sidebar__widgets .single__widget.Popular__classes ul li a:hover {
            color: {{$setting->color_theme}};
        }
        .sidebar__widgets .single__widget.Popular__classes ul li:hover::before {
            background: {{$setting->color_theme}};
        }
        a#scrollUp:hover {
            background:{{$setting->color_theme}};
        }
        .junior__header__top .jun__header__top__left .top__address a:hover {
            color:{{$setting->color_theme}};
        }
        .junior__header__top .jun__header__top__right .accounting li a:hover {
            color: {{$setting->color_theme}};
        }
        .header--2 .shopbox a {
            color: {{$setting->color_theme}};
        }
        .header--3 .shopbox a {
            color: {{$setting->color_theme}};
        }
        .blog .blog__date {
            background: {{$setting->color_theme}} ;
        }
        .blog .blog__inner ul.blog__meta {
            background: {{$setting->color_theme}} ;
        }
        .slide__carosel.owl-theme .owl-nav [class*="owl-"]:hover {
            background: {{$setting->color_theme}} none repeat scroll 0 0;
        }
        .page-class-details .class__view blockquote::before {
            background:  {{$setting->color_theme}} none repeat scroll 0 0;
        }
        .footer--2 .footer__widget .ftr__latest__post .single__ftr__post .ftr__post__details h6 a:hover {
            color: {{$setting->color_theme}};
        }
        .hover-item:hover + h5.text-hover{
            color: {{$setting->color_theme}};
        }
        .bottom-line{
            background: {{$setting->color_theme}};
        }
        .hover-item:hover {
            transform: scale(1.3);
        }
        .sidebar__widgets .single__widget.tags ul li a:hover {
            background: {{$setting->color_theme}} none repeat scroll 0 0;
            border: 1px solid {{$setting->color_theme}};
        }
        .footer--2 .footer__wrapper.section-padding--lg {
           padding: 0 !important;
        }
        .footer__wrapper.section-padding--lg {
           padding: 0 !important;
        }
        .section-padding--lg {
             padding: 0 !important;
        }
        #footer{
            border-top: 2px solid {{$setting->color_theme}};
            margin-top: 25px;
        }
        .countbox.timer-grid div > span {
            background: {{$setting->color_theme}} !important;
        }
        .event__btn li:last-child {
            background: {{$setting->color_theme}} !important; ;
        }
        .galler__wrap{
            direction: rtl;
        }
        .gallery__item{
            position: relative !important;
            left: unset !important;
            top: unset !important;
        }
        .event__btn li:last-child:hover {
            background: {{$setting->color_theme}} !important; ;
        }
        .dcare__event .event__content .event__pub .event__date span {
            color: {{$setting->color_theme}} !important;
        }
        .dcare__event .event__content .event__pub .event__time li i {
            color: {{$setting->color_theme}} !important;
        }

        .bg-image--10 {
            @if(!$setting->Hasmedia('footer'))
            background-image: url({{asset('img/no-img.gif')}});
            @else
            background-image: url({{$setting->getFirstMediaUrl('footer')}});
            @endif

        }
        @media only screen and (max-width:767px) {
            .footer__wrapper.bg-image--10 {
                background:  {{$setting->color_theme}} !important;
                color: #fdfdfd;
            }
            video{
                width: 300px !important;
            }
            .title-video{
                font-size: 15px;
            }

            .footer--2 .footer__widget .ftr__latest__post .single__ftr__post .ftr__post__details h6 a {
                color: #fdfdfd;
            }
            .footer--2 .footer__widget h4 {
                color: #fdfdfd;
            }
            .footer--2 .footer__widget .ftr__latest__post .single__ftr__post .ftr__post__details span {
                color: #fdfdfd;
            }
            .footer--2 .footer__widget .ftr__details p {
                color: #fdfdfd;
            }
        }

        @media only screen and (max-width: 1100px)  {
            .footer-gallery h6 {
                font-size: 10px;
            }
            video{
                width: 300px !important;
            }
            .title-video{
                font-size: 14px;
            }
        }
    </style>

</head>
<body>
<!--[if lte IE 9]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
<![endif]-->

<!-- Add your site or application content here -->

<!-- <div class="fakeloader"></div> -->

<!-- Main wrapper -->
<div class="wrapper" id="wrapper">
    <!-- Header -->
    <header id="header" class="jnr__header header--one clearfix">
        <!-- Start Header Top Area -->
        <div class="junior__header__top">
            <div class="container">
                <div class="row" style="justify-content: space-between;">

                    <div class="col-md-5 col-lg-2 col-sm-12">
                        <div class="jun__header__top__right">
                            <ul class="accounting d-flex justify-content-lg-end justify-content-md-end justify-content-start align-items-center">
                                @if(!auth('client')->check())
                                    <li><a class="action-item"  href="{{route('client.dashboard')}}" style="border-radius: 9px;border: 2px solid {{$setting->color_theme}};padding: 10px;">ورود</a></li>
                                    <li><a class="action-item" href="{{route('client.register')}}" style="border-radius: 9px;border: 2px solid {{$setting->color_theme}};padding: 10px;">ثبت نام</a></li>
                                @else
                                    <li><a  class="action-item" href="{{route('client.dashboard')}}">پنل کاربری</a></li>
                                    <li><a class="action-item"  href="{{route('client.logout.panel')}}">خروج</a></li>
                                @endif
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-7 col-lg-6 col-sm-12">
                        <div class="jun__header__top__left">
                            <ul class="top__address d-flex justify-content-start align-items-center flex-wrap flex-lg-nowrap flex-md-nowrap">
                                <li><i class="fa fa-envelope"></i><a href="mailto::{{$setting->email}}">{{$setting->email}}</a></li>
                                <li><i class="fa fa-phone"></i><a href="tel::{{isset(json_decode($setting->phone,true)[1]) ? json_decode($setting->phone,true)[1] : ""}}">{{isset(json_decode($setting->phone,true)[1]) ? json_decode($setting->phone,true)[1] : ""}}</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Header Top Area -->
       @include('template.sections.menu')
    </header>
    <!-- //Header -->
