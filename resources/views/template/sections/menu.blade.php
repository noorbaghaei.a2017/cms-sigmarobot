<!-- Start Mainmenu Area -->
<div class="mainmenu__wrapper bg__cat--1 poss-relative header_top_line sticky__header">
    <div class="container">
        <div class="row d-none d-lg-flex">
            <div class="col-sm-4 col-md-6 col-lg-2 order-1 order-lg-1">
                <div class="logo">
                    <a href="{{route('front.website')}}">
                        @if(!$setting->Hasmedia('logo'))
                            <img src="{{asset('img/no-img.gif')}}" alt="{{$setting->name}}"  width="70">
                        @else

                            <img src="{{$setting->getFirstMediaUrl('logo')}}" alt="{{$setting->name}}" width="70">
                        @endif

                    </a>
                </div>
            </div>
            <div class="col-sm-4 col-md-2 col-lg-9 order-3 order-lg-2">
                <div class="mainmenu__wrap">
                    <nav class="mainmenu__nav">
                        <ul class="mainmenu">
                            @foreach($top_menus as $menu)
                            <li ><a href="{{$menu->href}}">{{$menu->symbol}}</a></li>
                            @endforeach
                        </ul>
                    </nav>
                </div>
            </div>

        </div>
        <!-- Mobile Menu -->
        <div class="mobile-menu d-block d-lg-none">
            <div class="logo">
                <a href="{{route('front.website')}}">
                    @if(!$setting->Hasmedia('logo'))
                        <img src="{{asset('img/no-img.gif')}}" alt="{{$setting->name}}" width="70">
                    @else

                        <img src="{{$setting->getFirstMediaUrl('logo')}}" alt="{{$setting->name}}" width="70">
                    @endif

                </a>
            </div>
        </div>
        <!-- Mobile Menu -->
    </div>
</div>
<!-- End Mainmenu Area -->
