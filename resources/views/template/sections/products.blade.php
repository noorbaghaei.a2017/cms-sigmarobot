@if(\Modules\Core\Helper\CoreHelper::hasProduct())
<div  id="section2">
    <section>
        <div class="container-fluid plp-container">
            <div class="plp-box">
                <strong class="section-title-inline-1">
                    <h1> Products </h1>
                </strong>
            </div>
        </div>

        <div class="container-fluid slider-container">
            <div class="pro-slide-container">
                <div class="product-carousel owl-carousel owl-theme">

                    @foreach($products as $product)

                        <div class="item">
                            <div class="image-carousel-content">
                                <div class="img-carousel">
                                    <a href="{{route('products.single',['product'=>$product->slug])}}">
                                    <img style="width: 250px;height: auto" src="{{$product->getFirstMediaUrl('images','thumb')}}" alt="">
                                    </a>
                                </div>
                            </div>
                            <div class="pro-title">
                                <a href="{{route('products.single',['product'=>$product->slug])}}">
                                    <div class="product_thumb_content"> {{$product->title}}</div>
                                </a>
                            </div>
                        </div>
                    @endforeach

                </div>
            </div>
        </div>

    </section>
</div>
@endif
