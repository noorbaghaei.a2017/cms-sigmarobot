<!-- Start About Area -->
<section class="about-area">
    <div class="container-fluid p-0">
        <div class="row m-0">
            <div class="col-lg-6 col-md-12 p-0">
                <div class="about-image">
                    <img src="{{asset('template/img/sepehri/leyla-sepehri.jpeg')}}" alt="image">
                </div>
            </div>

            <div class="col-lg-6 col-md-12 p-0">
                <div class="about-content">
                    <span>درباره ما</span>
                    <h2>ویژگی هایی در مورد کلینیک سپهری</h2>

                    <ul>
                        @foreach($properties as $property)
                            <li><i class="{{$property->icon}}"></i>{{$property->title}}</li>
                        @endforeach
                    </ul>


                    @if($more)
                        <a href="{{route('about-us')}}" class="btn btn-primary">بیشتر بدانید <i class="flaticon-left-chevron"></i></a>

                    @endif
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End About Area -->
