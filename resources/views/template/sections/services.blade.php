@if(\Modules\Core\Helper\CoreHelper::hasService())
<!-- Start Our Service Area -->
<section class="junior__service bg-image--1 section-padding--bottom section--padding--xlg--top">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section__title text-center">
                    <h2 class="title__line">خدمات</h2>
                </div>
            </div>
        </div>
        <div class="row" style="direction: rtl">
            @foreach($services as $service)
            <!-- Start Single Service -->
            <div class="col-lg-3 col-md-6 col-sm-6 col-12" >
                <div class="service bg--white border__color wow fadeInUp">
                    <div class="service__icon">
                        @if(!$service->Hasmedia('images'))
                            <img src="{{asset('img/no-img.gif')}}" alt="icon images">
                        @else

                            <img src="{{$service->getFirstMediaUrl('images')}}" alt="icon images" >
                        @endif

                    </div>
                    <div class="service__details">
                        <h6><a href="#">{{$service->title}}</a></h6>
                        <p>
                            {{$service->excerpt}}
                        </p>
                        <div class="service__btn">
                            <a class="dcare__btn btn__gray hover--theme min__height-btn" href="#">اطلاعات بیشتر</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Single Service -->
                @endforeach


        </div>
    </div>
</section>
@endif
<!-- End Our Service Area -->
