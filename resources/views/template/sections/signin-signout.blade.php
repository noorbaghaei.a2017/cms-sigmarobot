@if(!auth('client')->check())
<section id="sign-section" class="junior__welcome__area section-padding--md ">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-right">
                <div>
                    @include('core::layout.alert-danger')
                    @include('core::layout.alert-success')
                </div>
            </div>
        </div>
        <div class="row">

            <div class="col-lg-6 text-center">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="section__title text-center">
                            <h2 class="title__line">ثبت نام</h2>
                        </div>
                    </div>

                </div>
               <form action="{{route('client.register.submit.client')}}" method="POST">
                   @csrf

                  <div class="form-group">
                      <label >ایمیل</label>
                      <input class="form-control" type="email" name="email" value="{{old('email')}}" autocomplete="off">
                  </div>
                   <div class="form-group">
                       <label >پسورد</label>
                   <input class="form-control" type="password" name="password"  autocomplete="off">
                   </div>
                   <div class="form-group">
                       <input class="btn btn-block btn-success" type="submit" value="ثبت نام">

                   </div>
               </form>
            </div>
            <div class="col-lg-6 text-center">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="section__title text-center">
                            <h2 class="title__line">ورود</h2>
                        </div>
                    </div>
                </div>
                <form action="{{ route('client.login.submit') }}" method="POST">
                    @csrf

                    <div class="form-group">
                        <label >ایمیل</label>
                    <input type="email" class="form-control" name="email" value="{{old('email')}}" autocomplete="off">
                    </div>

                    <div class="form-group">
                        <label >پسورد</label>
                    <input class="form-control" type="password" name="password" autocomplete="off">
                    </div>
                    <div class="form-group">
                        <input class="btn btn-block btn-success" type="submit" value="ورود">

                    </div>
                </form>
            </div>
        </div>
    </div>


</section>
@endif
