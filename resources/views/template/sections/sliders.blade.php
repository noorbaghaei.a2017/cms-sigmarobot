<!-- Strat Slider Area -->
<div class="slide__carosel owl-carousel owl-theme">
    @foreach($sliders as $key=>$slider)
        @if(!$slider->Hasmedia('images'))
            <div class="slider-template slider__area bg-pngimage--{{$key+1}}  d-flex fullscreen justify-content-start align-items-center" style="background-image: url({{asset('img/no-img.gif')}})">
                @else
                    <div class="slider-template slider__area bg-pngimage--{{$key+1}}  d-flex fullscreen justify-content-start align-items-center" style="background-image: url({{$slider->getFirstMediaUrl('images')}})">
                        @endif
                        <div class="container">
                            <div class="row">
                            </div>
                        </div>
                    </div>
                    @endforeach

            </div>
</div>
