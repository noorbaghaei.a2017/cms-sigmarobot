@extends('template.app')

@section('content')
    <div class="hidden-text">

        <h1>آموزش رباتیک</h1>
        <h1>رباتیک دانش آموزی</h1>
    </div>
    <!-- Start Courses Area -->
    <section class="dcare__courses__area section-padding--lg bg--white" style="padding-top: 15px !important;">
        <div class="container">
            <div class="row class__grid__page" style="direction: rtl">
                <div>
                    @include('core::layout.alert-success')
                    @include('core::layout.alert-danger')
                </div>
                <!-- Start Single Courses -->
                @foreach($items as $item)
                    <div class="col-lg-3 col-md-5 col-sm-6 col-6">
                        <div class="courses">
                            <div class="courses__thumb">
                                <a href="{{route('single.students',['classroom'=>$item->id])}}">
                                    @if(!$item->Hasmedia('images'))
                                        <img src="{{asset('img/no-img.gif')}}" alt="courses images" width="370" height="170">
                                    @else

                                        <img src="{{$item->getFirstMediaUrl('images','medium')}}" alt="courses images" width="370" >
                                    @endif
                                </a>
                            </div>
                            <div class="courses__inner">
                                <div class="courses__wrap text-right">
                                    <div class="courses__content">
                                        <h6><a href="{{route('single.students',['classroom'=>$item->id])}}">{{$item->title}}</a></h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            @endforeach
            <!-- End Single Courses -->
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="dcare__pagination mt--80">
{{--                        {!! $items->links() !!}--}}
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Courses Area -->


@endsection



