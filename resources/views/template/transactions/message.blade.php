@extends('template.app')

@section('content')


    <!-- End Bradcaump area -->
    <!-- Start Choose Us Area -->
    <section class="dcare__choose__us__area section-padding--lg bg--white text-center">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-sm-12 col-md-12">
                    <div class="section__title text-center" style="padding-top: 25px">
                        <h2 class="title__line"> بازگشت از بانک</h2>
                    </div>
                </div>
            </div>
            <div class="row mt--40" style="justify-content: center">
                <!-- Start Single Choose Option -->
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="dacre__choose__option">
                        <!-- Start Single Choose -->

                       @if($error==1)

                            <h3 class="alert alert-success">{{$message}}</h3>

                       @elseif($error==2)

                            <h3 class="alert alert-danger">{{$message}}</h3>

                       @elseif($error==0)

                            <h3 class="alert alert-warning">{{$message}}</h3>

                       @endif

                        <div class="btn btn-success btn-block">
                            <a href="{{route('front.website')}}" style="color: #fdfdfd;">بازگشت از سایت</a>
                        </div>


                        <!-- End Single Choose -->

                    </div>
                </div>
                <!-- End Single Choose Option -->


            </div>
        </div>
    </section>
    <!-- End Choose Us Area -->



@endsection






