﻿@extends('template.sections.user.app')
@section('content')
    <!-- login wrapper start -->
    <div class="login_wrapper jb_cover">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="login_top_box jb_cover">
                        <div class="login_banner_wrapper">

                        </div>

                        <form action="{{ route('client.login.submit') }}" method="POST" class="login100-form validate-form p-l-55 p-r-55 p-t-178" style="direction: rtl">

                            @csrf

                            <div class="login_form_wrapper">
                                <div>
                                    @if(!$setting->Hasmedia('logo'))
                                        <img src="{{asset('img/no-img.gif')}}" alt="logo" width="80" height="60">
                                    @else
                                        <img src="{{$setting->getFirstMediaUrl('logo')}}" alt="logo" width="80" height="60">
                                    @endif
                                </div>
                                <h2>ورود</h2>
                                <div class="form-group icon_form comments_form">
                                    @error('email')
                                    <span class="invalid-feedback" role="alert" style="display: block">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                    @error('email')
                                    <span class="invalid-feedback" role="alert" style="display: block">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                    <input type="text" class="form-control require" name="identify" placeholder="موبایل یا ایمیل">
                                    <i class="fas fa-envelope"></i>
                                </div>
                                @error('password')
                                <span class="invalid-feedback" role="alert" style="display: block">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                <div class="form-group icon_form comments_form">

                                    <input type="password" class="form-control require" name="password" placeholder="رمزعبور *">
                                    <i class="fas fa-lock"></i>
                                </div>

                                <div class="header_btn search_btn login_btn jb_cover">

                                    <input class="btn btn-success btn-lg" type="submit" value="ورود">
                                </div>
                                <div class="dont_have_account jb_cover">
                                    <p>حساب کاربری ندارید؟ <a href="{{route('client.register')}}">ثبت نام</a></p>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- login wrapper end -->

    {{--    <form action="{{ route('client.login.submit') }}" method="POST" class="login100-form validate-form p-l-55 p-r-55 p-t-178" style="direction: rtl">--}}

{{--        @csrf--}}

{{--					<span class="login100-form-title">--}}
{{--						پنل کاربران--}}
{{--					</span>--}}
{{--        @error('email')--}}
{{--        <span class="invalid-feedback" role="alert" style="display: block">--}}
{{--                                        <strong>{{ $message }}</strong>--}}
{{--                                    </span>--}}
{{--        @enderror--}}
{{--        <div class="wrap-input100 validate-input m-b-16" data-validate="لطفا نام کاربری را وارد کنید">--}}
{{--            <input class="input100" type="text" name="email" placeholder="نام کاربری">--}}
{{--            <span class="focus-input100"></span>--}}
{{--        </div>--}}
{{--        @error('password')--}}
{{--        <span class="invalid-feedback" role="alert" style="display: block">--}}
{{--                                        <strong>{{ $message }}</strong>--}}
{{--                                    </span>--}}
{{--        @enderror--}}
{{--        <div class="wrap-input100 validate-input" data-validate = "لطفا کلمه عبور را وارد کنید">--}}
{{--            <input class="input100" type="password" name="password" placeholder="کلمه عبور">--}}
{{--            <span class="focus-input100"></span>--}}
{{--        </div>--}}


{{--        <div class="container-login100-form-btn">--}}
{{--            <button class="login100-form-btn">--}}
{{--                ورود--}}
{{--            </button>--}}
{{--        </div>--}}

{{--    </form>--}}
@endsection






