@extends('template.app')

@section('content')


    <div class="container text-right" style="margin-top: 30px">
        <div>
            @include('core::layout.alert-success')
            @include('core::layout.alert-danger')
        </div>
        <div class="row" style="direction: rtl">
            <div class="col-lg-12 ">
                <nav>
                    <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                        <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">پروفایل</a>
                        <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">کلاس ها</a>
                        <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">مسابفات</a>
                    </div>
                </nav>
                <div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
                    <div class="tab-pane fade show active container" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                       <div class="content-wrapper">
                           <form action="{{route('client.update',['client'=>$client])}}" method="POST">
                               @csrf
                               @method('PATCH')
                               <div class="form-group">
                                   <span class="text-danger">*</span>
                                   <label for="mobile">موبایل</label>
                                   <input type="text" value="{{$client->mobile}}" class="form-control" name="mobile" id="mobile">
                               </div>
                               <div class="form-group">
                                   <label for="mobile">ایمیل</label>
                                   <input type="email" value="{{$client->email}}" class="form-control" name="email" id="email">
                               </div>
                               <div class="form-group">

                                   <label for="first_name">نام</label>
                                   <input type="text" value="{{$client->first_name}}" class="form-control" name="first_name" id="first_name">
                               </div>
                               <div class="form-group">
                                   <label for="last_name">نام خانوادگی</label>
                                   <input type="text" value="{{$client->last_name}}" class="form-control" name="last_name" id="last_name">
                               </div>
                               <button type="submit" class="btn btn-success">بروز رسانی</button>
                           </form>
                       </div>
                    </div>
                    <div class="tab-pane fade container" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">نام</th>
                                <th scope="col">تاریخ شروع</th>
                                <th scope="col">نمره</th>
                                <th scope="col">امتیاز</th>
                                <th scope="col">رکورد</th>
                            </tr>
                            </thead>
                            <tbody>

                          @foreach($client->classrooms as $key=>$action)
                             @if(\Modules\Educational\Entities\ClassRoom::find($action->actionable_id)->parent==0)
                                 <tr>
                                     <th scope="row">{{$key + 1}}</th>
                                     <td>{{\Modules\Educational\Entities\ClassRoom::find($action->actionable_id)->title}}</td>
                                     <td>{{Morilog\Jalali\Jalalian::forge(\Modules\Educational\Entities\ClassRoom::find($action->actionable_id)->created_at)->format('%A, %d %B %y')}}</td>

                                     @if(hasChild(\Modules\Educational\Entities\ClassRoom::find($action->actionable_id),'classroom'))

                                         <td colspan="3"><a href="#" class="btn btn-block btn-success" data-toggle="modal" data-target="#detail-{{\Modules\Educational\Entities\ClassRoom::find($action->actionable_id)->token}}" data-ui-toggle-class="rotate" data-ui-target="#detail">لیست نمرات</a></td>


                                         <div id="detail-{{\Modules\Educational\Entities\ClassRoom::find($action->actionable_id)->token}}" class="modal fade animate" data-backdrop="true">
                                             <div class="modal-dialog" id="detail">
                                                 <div class="modal-content">

                                                     <div class="modal-header text-center">


                                                         <span>لیست نمرات</span>
                                                     </div>


                                                     <div class="modal-body text-center p-lg">

                                                         <div class="row">

                                                             <div class="col-lg-4">
                                                               <span>
                                                                   نام
                                                               </span>
                                                             </div>
                                                             <div class="col-lg-2">
 <span>
                                                                   نمره
                                                               </span>
                                                             </div>
                                                             <div class="col-lg-2">
 <span>
                                                                   امتیاز
                                                               </span>
                                                             </div>
                                                             <div class="col-lg-2">
 <span>
                                                                   رکورد
                                                               </span>


                                                             </div>
                                                             <div class="col-lg-2">
 <span>
                                                                   رتبه
                                                               </span>
                                                             </div>
                                                         </div>

                                                         @foreach(getChild(\Modules\Educational\Entities\ClassRoom::find($action->actionable_id),'race') as $child)
                                                             <div class="row">

                                                                 <div class="col-lg-4">
                                                               <span>
                                                                   {{$child->title}}
                                                               </span>
                                                                 </div>
                                                                 <div class="col-lg-2">
 <span>
                                                                      {{\Modules\Core\Entities\UserAction::where('actionable_id',$child->id)->where('actionable_type',\Modules\Educational\Entities\ClassRoom::class)->whereClient($client->id)->first()->score}}
                                                               </span>
                                                                 </div>
                                                                 <div class="col-lg-2">
 <span>
                                                                    {{\Modules\Core\Entities\UserAction::where('actionable_id',$child->id)->where('actionable_type',\Modules\Educational\Entities\ClassRoom::class)->whereClient($client->id)->first()->rate}}

                                                               </span>
                                                                 </div>
                                                                 <div class="col-lg-2">
 <span>
                                                                       {{\Modules\Core\Entities\UserAction::where('actionable_id',$child->id)->where('actionable_type',\Modules\Educational\Entities\ClassRoom::class)->whereClient($client->id)->first()->record}}

                                                               </span>
                                                                 </div>

                                                                 <div class="col-lg-2">
 <span>

                                                               </span>
                                                                 </div>

                                                             </div>

                                                         @endforeach

                                                     </div>
                                                     <div class="modal-footer">


                                                         <button class="btn btn-danger p-x-md" data-dismiss="modal">{{__('cms.ok')}}</button>
                                                     </div>
                                                 </div>
                                             </div>
                                         </div>
                                     @else

                                     @if(is_null($action->score))


                                         <td></td>
                                     @else
                                         <td>{{$action->score}}</td>
                                     @endif

                                     @if(is_null($action->rate)))

                                     <td></td>

                                     @else
                                         <td>{{$action->rate}}</td>
                                     @endif
                                         @if(is_null($action->record)))

                                         <td></td>

                                         @else
                                             <td>{{$action->record}}</td>
                                         @endif

                                     @endif
                                 </tr>

                             @endif
                          @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="tab-pane fade container" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">

                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">نام</th>
                                <th scope="col">تاریخ شروع</th>
                                <th scope="col">نمره</th>
                                <th scope="col">امتیاز</th>
                                <th scope="col">رکورد</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($client->races as $key=>$action)
                                @if(\Modules\Educational\Entities\Race::find($action->actionable_id)->parent==0)
                                <tr>
                                    <th scope="row">{{$key + 1}}</th>
                                    <td>{{\Modules\Educational\Entities\Race::find($action->actionable_id)->title}}</td>
                                    <td>{{Morilog\Jalali\Jalalian::forge(\Modules\Educational\Entities\Race::find($action->actionable_id)->created_at)->format('%A, %d %B %y')}}</td>

                                    @if(hasChild(\Modules\Educational\Entities\Race::find($action->actionable_id),'race'))

                                        <td colspan="3"><a href="#" class="btn btn-block btn-success" data-toggle="modal" data-target="#detail-{{\Modules\Educational\Entities\Race::find($action->actionable_id)->token}}" data-ui-toggle-class="rotate" data-ui-target="#detail">لیست نمرات</a></td>


                                            <div id="detail-{{\Modules\Educational\Entities\Race::find($action->actionable_id)->token}}" class="modal fade animate" data-backdrop="true">
                                                <div class="modal-dialog" id="detail">
                                                    <div class="modal-content">

                                                            <div class="modal-header text-center">


                                                                <span>لیست نمرات</span>
                                                            </div>


                                                        <div class="modal-body text-center p-lg">

                                                           <div class="row">

                                                               <div class="col-lg-4">
                                                               <span>
                                                                   نام
                                                               </span>
                                                               </div>
                                                               <div class="col-lg-2">
 <span>
                                                                   نمره
                                                               </span>
                                                               </div>
                                                               <div class="col-lg-2">
 <span>
                                                                   امتیاز
                                                               </span>
                                                               </div>
                                                               <div class="col-lg-2">
 <span>
                                                                   رکورد
                                                               </span>


                                                            </div>
                                                               <div class="col-lg-2">
 <span>
                                                                   رتبه
                                                               </span>
                                                               </div>
                                                           </div>

                                                            @foreach(getChild(\Modules\Educational\Entities\Race::find($action->actionable_id),'race') as $child)
                                                            <div class="row">

                                                                <div class="col-lg-4">
                                                               <span>
                                                                   {{$child->title}}
                                                               </span>
                                                                </div>
                                                                <div class="col-lg-2">
 <span>
                                                                      {{\Modules\Core\Entities\UserAction::where('actionable_id',$child->id)->where('actionable_type',\Modules\Educational\Entities\Race::class)->whereClient($client->id)->first()->score}}
                                                               </span>
                                                                </div>
                                                                <div class="col-lg-2">
 <span>
                                                                    {{\Modules\Core\Entities\UserAction::where('actionable_id',$child->id)->where('actionable_type',\Modules\Educational\Entities\Race::class)->whereClient($client->id)->first()->rate}}

                                                               </span>
                                                                </div>
                                                                <div class="col-lg-2">
 <span>
                                                                       {{\Modules\Core\Entities\UserAction::where('actionable_id',$child->id)->where('actionable_type',\Modules\Educational\Entities\Race::class)->whereClient($client->id)->first()->record}}

                                                               </span>
                                                                </div>

                                                                <div class="col-lg-2">
 <span>

                                                               </span>
                                                                </div>

                                                            </div>

                                                            @endforeach

                                                        </div>
                                                        <div class="modal-footer">


                                                            <button class="btn btn-danger p-x-md" data-dismiss="modal">{{__('cms.ok')}}</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                    @else

                                    @if(is_null($action->score))


                                        <td></td>
                                    @else
                                        <td>{{$action->score}}</td>
                                    @endif

                                    @if(is_null($action->rate)))

                                    <td></td>

                                    @else
                                        <td>{{$action->rate}}</td>
                                    @endif

                                        @if(is_null($action->record)))

                                        <td></td>

                                        @else
                                            <td>{{$action->record}}</td>
                                        @endif

                                    @endif

                                </tr>
                                @endif
                            @endforeach

                            </tbody>
                        </table>
                    </div>

                </div>

            </div>
        </div>
    </div>
    </div>



@endsection

@section('head')

    <style>
        th,
        td{
            text-align:right;
        }
        nav > .nav.nav-tabs{

            border: none;
            color:#fff;
            background:#272e38;
            border-radius:0;

        }
        nav > div a.nav-item.nav-link,
        nav > div a.nav-item.nav-link.active
        {
            border: none;
            padding: 18px 25px;
            color:#fff;
            background:#272e38;
            border-radius:0;
        }

        nav > div a.nav-item.nav-link.active:after
        {
            content: "";
            position: relative;
            bottom: -60px;
            left: 9%;
            border: 15px solid transparent;
            border-top-color: #0695e8 ;
        }
        .tab-content{
            background: #fdfdfd;
            line-height: 25px;
            border: 1px solid #ddd;
            border-top:5px solid #0695e8;
            border-bottom:5px solid #0695e8;
            padding:30px 25px;
        }

        nav > div a.nav-item.nav-link:hover,
        nav > div a.nav-item.nav-link:focus
        {
            border: none;
            background: #0695e8;
            color:#fff;
            border-radius:0;
            transition:background 0.20s linear;
        }

    </style>

@endsection


