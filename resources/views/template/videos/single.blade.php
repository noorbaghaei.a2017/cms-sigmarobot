@extends('template.app')

@section('content')
    <div class="hidden-text">

        <h1>آموزش رباتیک</h1>
        <h1>رباتیک دانش آموزی</h1>
    </div>
    <!-- Start Our Gallery Area -->
    <div class="junior__gallery__area gallery-page-one gallery__masonry__activation gallery--3 bg-image--25 section-padding--lg" style="padding-top: 15px !important;">
        <div class="container">

            <div class="row galler__wrap masonry__wrap mt--80">
                @foreach($items as $item)
                    <div class="col-lg-6 col-md-6 col-sm-12 col-12 gallery__item gallery--{{$item->id}}" >
                        <div class="gallery" style="padding: 0 5px">
                            <div class="gallery__thumb" style="margin-top: 30px;display: inline-block">
                                @if(!$item->Hasmedia('images')))
                                <video controls poster="{{asset('img/no-img.gif')}}" width="550">
                                    <source  src="#" type="video/mp4">
                                </video>
                                @else
                                    <video controls poster="{{$item->getFirstMediaUrl('images')}}" width="550">
                                    <source src="{{$item->getFirstMediaUrl('videos')}}" type="video/mp4">
                                    </video>

                                @endif
                                    <h4 class="title-video" style="margin-top: 5px">{{$item->title}}</h4>
                            </div>

                        </div>
                        <div class="footer-gallery">
                            <a target="_blank" href="{{$item->href}}">
                                <span class="more-click btn ">برای دیدن ادامه فیلم کلیک کنید</span>
                            </a>
                        </div>
                    </div>

                    @endforeach


            </div>
        </div>
    </div>
    <!-- End Our Gallery Area -->


@endsection



