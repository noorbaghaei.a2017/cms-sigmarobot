<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



use Illuminate\Support\Facades\Route;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

Route::group(['prefix' => LaravelLocalization::setLocale(),'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]], function()
{
    if(env('SET_PREFIX')){
        Route::get('/index', 'HomeController@website')->name('front.website');
        Route::get('/', 'HomeController@prefix')->name('prefix');
    }
    else{
        Route::get('/', 'HomeController@website')->name('front.website');
    }

    Route::get('/generate/site-map', 'HomeController@generateSiteMap')->name('generate.sitemap');
    Route::get('/articles', 'HomeController@articles')->name('articles');
    Route::get('/students', 'HomeController@students')->name('students');
    Route::get('/students/single/{classroom}', 'HomeController@singleStudents')->name('single.students');
    Route::get('/galleries', 'HomeController@galleries')->name('galleries');
    Route::get('/galleries/{category}', 'HomeController@singleGallery')->name('galleries.single');
    Route::get('/videos', 'HomeController@videos')->name('videos');
    Route::get('/videos/{category}', 'HomeController@singleVideo')->name('videos.single');
    Route::get('/articles/tag/{tag}', 'HomeController@articlesTag')->name('articles.tag');
    Route::get('/articles/category/{category}', 'HomeController@articlesCategory')->name('articles.category');
    Route::get('/advertisings', 'HomeController@advertisings')->name('advertisings');
    Route::get('/plans', 'HomeController@plans')->name('plans');
    Route::get('/courses', 'HomeController@courses')->name('courses');
    Route::get('/races', 'HomeController@races')->name('races');
    Route::get('/shop', 'HomeController@shop')->name('shop');
    Route::get('/shop/filter', 'HomeController@shopFilter')->name('filter.shop');
    Route::get('/company/filter', 'HomeController@companyFilter')->name('filter.company');
    Route::get('/advert/filter', 'HomeController@advertFilter')->name('filter.advert');
    Route::get('/articles/{article}', 'HomeController@singleArticle')->name('articles.single');
    Route::get('/events/{event}', 'HomeController@singleEvent')->name('events.single');
    Route::get('/advertisings/{advertising}', 'HomeController@singleAdvertising')->name('advertisings.single');
    Route::get('/courses/{course}', 'HomeController@singleCourse')->name('courses.single');
    Route::get('/races/{race}', 'HomeController@singleRace')->name('races.single');
    Route::get('/informations', 'HomeController@informations')->name('informations');
    Route::get('/companies', 'HomeController@companies')->name('companies');
    Route::get('/search/companies/{category}', 'HomeController@searchCompany')->name('search.companies');
    Route::get('/companies/{company}', 'HomeController@singleCompany')->name('companies.single');
    Route::get('/informations/{information}', 'HomeController@singleInformation')->name('informations.single');
    Route::get('/portfolios', 'HomeController@portfolios')->name('portfolios');
    Route::get('/portfolios/{portfolio}', 'HomeController@singlePortfolio')->name('portfolios.single');
    Route::get('/products', 'HomeController@products')->name('products');
    Route::get('/search/products/{category}', 'HomeController@searchProduct')->name('search.page.product');
    Route::get('/contact-us', 'HomeController@contactUs')->name('contact-us');
    Route::get('/category-advertising', 'HomeController@categoryAdvertising')->name('category-advertising');
    Route::get('/about-us', 'HomeController@aboutUs')->name('about-us');
    Route::get('/downloads', 'HomeController@downloads')->name('downloads');
    Route::get('/events', 'HomeController@events')->name('events');
    Route::get('/products/{product}', 'HomeController@singleProduct')->name('products.single');
    Route::get('/special', 'HomeController@specialProducts')->name('special.products');
    Route::get('/properties/{property}', 'HomeController@singleProperty')->name('properties.single');
    Route::get('/advantages/{advantage}', 'HomeController@singleAdvantage')->name('advantages.single');
    Route::get('/products/categories/{category}', 'HomeController@categoriesProduct')->name('categories.product');
    Route::get('/members/{member}', 'HomeController@singleMember')->name('members.single');
    Route::get('/page/{page}', 'HomeController@page')->name('page');
    Route::post('/send/cv/{token}', 'HomeController@sendCv')->name('send.cv')->middleware('auth:client');
    Route::get('/idea/send/contact', 'HomeController@sendIdea')->name('send.idea');
    Route::get('/panel/user/logout', 'HomeController@logout')->name('client.logout.panel');
    Route::get('/register/classroom/{classroom}', 'HomeController@RegisterClassroom')->name('register.registerClassroom');
    Route::get('/register/race/{race}', 'HomeController@RegisterRace')->name('register.registerRace');

    Route::group(["prefix"=>"cart"],function() {

        Route::get('/add/product/{product}', 'HomeController@addToCart')->name('add.cart.product')->middleware('auth:client');
    });



    Route::group(["prefix"=>"country"],function() {

        Route::get('/get/api/country/all', 'HomeController@allCountry')->name('get.all.country');
        Route::get('/get/api/country/self/states/{country}', 'HomeController@getStates')->name('get.self.states');
        Route::get('/get/api/country/self/city/{state}', 'HomeController@getCities')->name('get.self.cities');

    });


    Route::group(["prefix"=>"hair"],function() {
        Route::get('/male', 'HomeController@maleHair')->name('male.hair');
        Route::post('/male/send/request', 'HomeController@sendRequestMale')->name('send.request.male');
        Route::get('/female', 'HomeController@femaleHair')->name('female.hair');
        Route::get('/result', 'HomeController@resultHair')->name('request.hair');
        Route::post('/female/send/request', 'HomeController@sendRequestFemale')->name('send.request.female');
    });


    Route::group(["prefix"=>"user/panel"],function() {
        Route::get('/welcome', 'HomeController@welcome')->name('client.welcome')->middleware('auth:client');
        Route::get('/dashboard', 'HomeController@panel')->name('client.dashboard')->middleware('auth:client');
        Route::get('/send/message', 'HomeController@sendChat')->name('client.send.chat')->middleware('auth:client');
        Route::get('/favorite', 'HomeController@favorite')->name('client.favorite')->middleware('auth:client');
        Route::get('/seekers', 'HomeController@seekers')->name('client.seekers')->middleware('auth:client');
        Route::get('/cvs', 'HomeController@cvs')->name('client.cvs')->middleware('auth:client');
        Route::get('/wallet', 'HomeController@wallet')->name('client.wallet')->middleware('auth:client');
        Route::get('/advertisings/cvs', 'HomeController@adverts')->name('client.advertisings')->middleware('auth:client');
        Route::get('/adverts/cvs', 'HomeController@advertCvs')->name('client.advert')->middleware('auth:client');
        Route::get('/plans', 'HomeController@clientPlans')->name('client.plans')->middleware('auth:client');
        Route::get('/self-information', 'HomeController@selfInformation')->name('client.self.information')->middleware('auth:client');
        Route::get('/edit', 'HomeController@edit')->name('client.edit')->middleware('auth:client');
        Route::get('/cart/list', 'HomeController@showCart')->name('client.show.cart')->middleware('auth:client');
        Route::get('/shopping', 'HomeController@showShopping')->name('client.show.shopping')->middleware('auth:client');
        Route::post('/register/receiver', 'HomeController@registerReceiver')->name('register.receiver.information.product')->middleware('auth:client');
        Route::get('/payment/shopping', 'HomeController@showPayment')->name('client.show.payment')->middleware('auth:client')->middleware('check.address.product');
        Route::get('/orders', 'HomeController@showOrders')->name('client.show.orders')->middleware('auth:client');
        Route::get('/orders/return', 'HomeController@showOrdersReturn')->name('client.show.orders.return')->middleware('auth:client');
        Route::get('/change/password/secret', 'HomeController@showChangePassword')->name('client.show.change.password')->middleware('auth:client');
        Route::patch('/change/password/secret', 'HomeController@changePassword')->name('client.change.password')->middleware('auth:client');
        Route::patch('/update/{client}', 'HomeController@update')->name('client.update')->middleware('auth:client');
        Route::patch('/update/seeker/{seeker}', 'HomeController@updateSeeker')->name('seeker.update')->middleware('auth:client');
        Route::get('/cart', 'HomeController@cart')->name('client.cart')->middleware('auth:client');
        Route::post('/add/advert', 'HomeController@addAdvert')->name('add.advert.employer')->middleware('auth:client');
    });

});



Route::group(["prefix"=>"transaction"],function() {
    Route::post('/send/request/{name}/{item?}', 'HomeController@sendTransactionRequest')->name('send.request.transaction');
    Route::get('/callback/request', 'HomeController@callbackTransaction')->name('transaction.callback');
    Route::get('/data/request/{error}/{payment}', 'HomeController@callbackTransactionMessage')->name('transaction.message.callback');
});


Route::group(["prefix"=>"ajax"],function() {
    Route::post('/verify/{user}/{code}', 'AjaxController@verifyMobileSms')->name('verify.mobile.sms.ajax');
    Route::post('/show/message', 'AjaxController@showMessage')->name('show.message.ajax');
    Route::get('/load/countries', 'AjaxController@loadCountries')->name('load.countries.ajax');
    Route::get('/load/country/states/{country}', 'AjaxController@loadStates')->name('load.states.ajax');
    Route::get('/cities/{state}', 'AjaxController@loadCities')->name('load.cities.ajax');
    Route::get('/areas/{city}', 'AjaxController@loadAreas')->name('load.areas.ajax');
    Route::post('/count/ad/{ad}', 'AjaxController@countAd')->name('count.ad.ajax');
    Route::get('/increment/product/{token}', 'AjaxController@incProduct')->name('inc.product.ajax');
    Route::get('/decrement/product/{token}', 'AjaxController@decProduct')->name('dec.product.ajax');
    Route::get('/remove/product/{token}', 'AjaxController@removeProduct')->name('dec.product.ajax');
});






